<!DOCTYPE html>
<html>
<head>
    <title>Rolebased</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- stylesheet	 -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/video/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <!--AOS animation css-->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>

    <link rel="stylesheet" type="text/css" href="{{asset('css/video/mystyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <!--<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css">-->
    <!-- Animate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


    <!-- End  of stylesheet -->
    <style type="text/css">
        .register-page header{
            border-bottom: 4px solid #f37121;
            box-shadow: 1px 1px 10px 1px grey;
            padding: 5px;
            /*background: #d2d2d2;*/
            margin: 0px;
        }
        .register-page #login .container #login-row #login-column #login-box {
            margin-top: 15px;
            max-width: 600px;
            /* height: 320px; */
            padding-top: 15px;
            border: 1px solid #9C9C9C;
            background-color: transparent;
            border-radius: 10px
        }
        .register-page #login .container #login-row #login-column #login-box #login-form {
            padding: 5px 20px;
        }
        .register-page #login .container #login-row #login-column #login-box #login-form #register-link {
            margin-top: -45px;
            text-align: right;
            padding: 12px 0px 0px;
        }
        .register-page section#login {
            /*height: 100vh;*/
            padding-bottom: 0px;
        }
        .register-page h3.text-center.text-white.pt-5 {
            padding-top: 120px;
            margin: 0px;
        }
        .register-page .form-control{
            background: transparent;
        }
        .register-page .text-info {
            color: #5d5d5d;
        }
        input.btn.submit-btn.btn-info.btn-md {
            width: 150px;
            padding: 10px;
            background: #f5700c;
            border: none;
            margin-top: 20px;
        }
        .help-block_message{
            color: red;
            font-size: larger;
        }
        .accept-checkbox input {
            -webkit-appearance: checkbox;
            cursor: pointer;
        }
        /*.info-tooltip{
                margin-top: -45px;
            text-align: right;
        }*/
        .info-tooltip{
            border: 0px;
            background: transparent;
        }
        #info-modal .modal-dialog {
            left: 0px;
        }
        .info-modal .close-modal{
            margin-top: -2px;
            font-size: 30px;
            float: right;
            font-size: 21px;
            font-weight: bold;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: 1;
            border: 0px;
            background: transparent;
            font-size: 25px;
            outline: none;
        }
        .info-modal {
            z-index: 999999;
        }
        .form-group.form-check.accept-checkbox {
            margin-bottom: 5px;
            font-size: 12px;
        }
        .info-modal-outer .info-modal-click{
            font-size: 12px;
        }
        .info-modal-outer .info-modal-click img, .info-tooltip img{
            width: 15px;
            margin-left: 5px;
        }
        .info-modal h5{
            font-weight: 600;
        }
        .info-modal {
            font-size: 13px;
        }
        .require:after{
            content:'*';
            color:red;
        }
        .imagesize{
            width: 20px;
        }
        div#login-column{
            height:800px
        }

        input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0;
        }
        .margin-bottom-0{
            margin-bottom: 0px;
        }
        .omb_login .omb_authTitle {
            text-align: center;
            line-height: 200%;
            margin-top: 20px;
            margin-bottom: 5px;

        }

        .omb_socialButtons {
            padding-bottom: 20px;
        }
        footer#footer {
            position: unset;
            bottom: 0;
            width: 100%;
        }
        body{
            background-color: #ffffff;
        }
        .margin-bottom-15{
            margin-bottom: 15px;
        }
        .mobilecountrycode div{
            padding-left: 0px;
        }
        .padding-right-0{
            padding-right: 0px;
        }
        .headermain{
            height: 83px;
        }

        @media(max-width:768px){
            nav.main_navigation, footer {
                display: none;
            }
        }
    </style>

</head>
<body class="register-page">


<header id="header" class="headermain">
    <div class="logo">
        <a class="navigation_link" href="{{url('/')}}"><img src="{{asset('role_based_image.png')}}" alt=""
                                                            class="white_logo"></a>
        <a class="navigation_link" href="{{url('/')}}"> <img src="{{asset('role_based_image.png')}}" alt=""
                                                             class="orange_logo"></a>
    </div>
</header><!-- /header -->

<section class="section" id="login">

    <h3 class="text-center text-white pt-5">Register</h3>
    <div class="container">
        @if ($message = Session::get('error_message'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ $message }}
            </div>
        @endif
        @if (session('flash_message'))
            <span class="alert alert-success">
                          {{ session('flash_message') }}
                        </span>
        @endif
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6 col-md-offset-3">
                <div id="login-box" class="col-md-12">

                    <form class="form" id="login-form" method="POST" action="{{ route('client.register') }}">
                        {{ csrf_field() }}
                        {{--<h3 class="text-center text-info">Register</h3>--}}
                        <div class="form-group">
                            <label for="name" class="text-info require">Name: </label><br>
                            <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                {!! $errors->first('name', '<p class="help-block_message">:message</p>') !!}
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email" class="text-info require">Email: </label><br>

                            <input type="email" name="email" id="email" class="form-control" value="{{old('email')}}">
                            @if ($errors->has('email'))
                                {!! $errors->first('email', '<p class="help-block_message">:message</p>') !!}
                            @endif
                        </div>
                        <div class="form-group mobilecountrycode">
                            <label for="mobile" class="text-info require">Mobile: </label><br>
                            <div class="col-md-3 col-xs-5 margin-bottom-15">
                                <select name="international_code" id="international_code" class="form-control" value="{{old('international_code')}}">
                                    @foreach(App\Models\MobileCode::get() as $key =>$value)
                                        <option value="{{$value->international_code}}" @if($value->id==101) {{'selected'}} @endif>{{$value->country_code.'-'.$value->international_code}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-9 col-xs-7  margin-bottom-15 padding-right-0">
                                <input type="number" name="mobile" id="mobile" class="form-control" value="{{old('mobile')}}">
                            </div>
                            @if ($errors->has('mobile'))
                                {!! $errors->first('mobile', '<p class="help-block_message">:message</p>') !!}
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info require">Password: </label><br>
                            <input type="password" name="password" id="password" class="form-control">
                            @if ($errors->has('password'))
                                {!! $errors->first('password', '<p class="help-block_message">:message</p>') !!}
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="text-info require">Confirm Password: </label><br>
                            <input type="password" name="password_confirmation" id="password-confirm" class="form-control">
                        </div>
                        <div class="form-group form-check accept-checkbox">
                            <input type="checkbox" class="form-check-input" id="exampleCheck111" name="term&condition"  value="1" @if(old('term&condition')==1) {{'checked'}} @endif required>
                            @if ($errors->has('term&condition'))
                                {!! $errors->first('term&condition', '<p class="help-block_message">:message</p>') !!}
                            @endif
                            <label class="form-check-label require" for="exampleCheck1">I accept the terms and conditions </label>
                        </div>

                        <div class="form-group info-modal-outer">
                            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#info-modal">Open Modal</button> -->

                            <!-- info-Modal starts-->


                            <!-- info-Modal ends-->


                        </div>
                        <div class="form-group margin-bottom-0">
                            <!-- <input type="checkbox" name="vehicle1" value="Bike"> I have a bike<br>
                            <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> -->
                            <input type="submit" name="submit" class="btn submit-btn btn-info btn-md" value="Register" id="register_submit">
                        </div>
                        <div id="register-link" class=" form-group">
                            <a href="{{url('client/login')}}" class="text-info">Login here</a>
                        </div>
                    </form>
                    <div class="omb_login">
                        <h3 class="omb_authTitle">Or Login With</h3>
                        <div class="row omb_row-sm-offset-3 omb_socialButtons">
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-facebook">
                                    <i class="fa fa-facebook visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-facebook"></span></span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-twitter">
                                    <i class="fa fa-twitter visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-twitter"></span></span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-google">
                                    <i class="fa fa-google-plus visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-google-plus"></span></span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-linkedin">
                                    <i class="fa fa-linkedin visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-linkedin"></span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer id="footer" class="" >
    <div class="footer_block">
        <div class="container">
            <div class="col-md-7">
                <div class="footer_logo col-md-4">
                    <img src="{{asset('role_based_image.png')}}" alt="">
                </div>
                <div class="copyright col-md-8">
                    <p>
                        Copyright @ 2019 Role Based
                    </p>
                </div>
            </div>
            <div class="col-md-5 text-right">
            </div>
        </div>
    </div>
</footer>
</body>
</html>

