<style>
    .header-user1{
        height: 40px;
        width: 40px;
        border-radius: 40px;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Start Role Based</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <?php
                $checkclient = Auth::guard('client')->user();

                ?>
                @if(Auth::guard('client')->check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-padding" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{--<i class="fas fa-user header-user"></i>--}}
                            @if($checkclient->image)
                                @if(strstr($checkclient->image,'https://')==true || strstr($checkclient->image,'data:image')==true)
                                    <img src="{{$checkclient->image}}" class="people-img header-user1">
                                @else
                                    <img src="{{asset('images/user_profile_image/'.$checkclient->image)}}"
                                         class="people-img header-user1">
                                @endif
                            @else
                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhAVFRUVFxUVFxUVFQ8VFRUVFRUWFxUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0fHR8rLS0tKystLS0tLS0tKy0rLS0tLS0tLS0tLS0tLS0tLS0tLTctLS0tMi0tLS03LSstK//AABEIAMIBAwMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xAA6EAABAwIEAwYDCAICAwEBAAABAAIRAwQFEiExQVFhBiJxgZGxEzKhByNCUnLB0fAU8WLhM4KSshb/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAkEQACAgICAQQDAQAAAAAAAAAAAQIRAyESMQQiMkFRM2FxQv/aAAwDAQACEQMRAD8ATbjut8B7KQwJq3Hdb4D2T7QvSSPPbDASwEQCWAgVhtS2pICWGoopMU1KRQjSBsCUGpKcaEEhZEMicDUxfXIpMLj5dSldFJWR7+uWAlomFUnEXuPIASTMacVX4jjToID/AJZOwgtO8eCqTicU3RsTMjfLBmfMrGTtnXCPFF5VvnEFuXXc8NJgefFM1XFrJnWCTBG/Ty4KnfizxldmJMddoRPx57qYBgmDEjQjgehUot2i0rXhaAdY0PVvj9Uwccpg5Kgh3BwGmpjblKphivxWSCWuA26cR1UY1mVhDhlc3kZBG2n8fVOgsvq98T8jusTx/wCJ/ZRcM7VPZVDah0Bg+cf79VmnvGsHK4bGTB05KJcXpqDvAEjZ2xjkk3xJdM7lYV/iNlSoWZ+z6uXWoc4ySd/AALTwt4u1ZySVSElFCWgnQhGVAhOIigBpEUooiqRI2SkkpZCQQgBD3KPVKkPCj1AgCMUEZCJFjskUG91vgPZPtakWw7rfAeyeQnomgkYCUjaEAG1qchBgSiUNlISjASgxKyJCYQalNCMNS2hKxpBhUfaLUsE7SddtRGqvCsnj2IND6jSZgtbxABgaeKib0a41cjI45dZCI08PRU9Jr8rjlOXXwHH+fVbbsx2aF7XAfOTj5azC6wzsbatZlFJuWI1A91zSlbO6Mfs81f5Dvl4ZY+slNXF+dOEAD0XYe1HYBrZNJoLfmGm3MH+VzLE+ztRpIjZxHA6eKjkynG0Z8XJBMHefruibXImCpFxhz2/hKsLDs1WqbNO8Tw8k+RnwZTOeUhbC37B13HUQCTAO/QwqztL2edaOaHAw7nr6ouxcGT+zV5UoNDmPPMt4Rz8F07BcUbcU87dwYcOR/uq41ZYpk0yiOK2/2dXodVqNB0LZjqDv6FdGOXwY5Y6s3yCOEIW5y2EiRlJASAQ5EjKJMQElwSkTkwGHBMvapBSXBJiRDLEaeIRpWacQrYd1vgPYJ4Jm3+VvgPZPgJrozDATjWomhOtCBhNCUAjARosYEsJCVmTEKlHKblKak0MWsBiVdzrmszL8rhqYiCNI8lv2tJ0Ak9N1g+0bjTxAtLXd9rSBoBI39j6LHNpHR469Ru/s9pFp1jZbq4rABY/sbQcBn4HgtJWfK4XI7+JEvboTHCFlcTw5lQ5tuY/eFobhiqq7VlKTNYozxwKlMujRT7NtKnoGiPBSRbEpqpZRxSUpFuKJ4umnUBYv7QrVtSiXO3Go6LSsblCx3bS67patORm4rZyolbv7LKc1y7kwg+f+gsLVGq6V9kVn3a1UjctYD4CTr5hdWLs4Muos6GQgUopLl2HGILURSsyIoAaekpbwkIEBEUaIpWA25JcnCEhwTAZJRoy1EkVYLcd1vgPZSGtQt6fdb+keykNpoUtE0NhqdCUGIiEDoARIIJisCOESW0IAJrUtoQhAJgO0bl1LNUaAXMY9wnaQ0lZv7RbQ1atlcZMj3lrXAn8wka9JI8lprFgdUaCJBMHwIgrP3lerWuG/EILadcQORJLRHj+y87ym1kX7PU8KKeN/o6RY2zadNo0ENE+MaqFUxShmI+K0RvqEntJeMp0XPqTka1znAcmiT4rhfaHtBTrvaKdm9hc6GA1Hsc4kwCGAQsjU7bcXlJ3yvB8CobnNK4I68ubeqZfVY4btc4nT2W37J4rWrNdUc6WtkDx4yspaNYbOhVXta3TioRqk8FzS/wC3FVjiwN1aSPJRbft/cT3m6D8v/aFFsJSS0dUbSnVYPt7YPPebw38FY4X2yNSO6TtIgAhW1ctqtJjgR12V1RPK0cM+ES4AbkwPEmF3vsxg4trSmwEF0ZniNcztSfCdFy3svSay4qVHBs0yQ3NEZiTqOoDT6ro3Zpjy6rVc8ua9tPLvAGpIHmt8WT1qKObLhvG5MvUh6WkOC76PMEII0UJjYElwSkCgQyURCccEhACSkEpZRFqAGyUSVkQSKJts3uN/S32CeDUzanuN/S32CksCzvRTE5U25qlZUlzUKQiJCMNT5YkEK7JoQGI0qEkuVIQZKKURKLMiwJNlUh7TycE7jmGmm/Mxs5n0nVIgkFpMEDltKhU2kmAJJ2AWht8bpvr/AOM4s+P8MvIBkgNLQQfULk8qF0zt8OfG19knELQVWQYI4giZHKFzztfSrU3AspExsQxrsvhIMLpNK5Gyg4q9jlxM7490cNfhd5du1pRO7ntDdPQLpvZTsv8AAtXMMHNvw1jdW2HNp5oMdFfVBlaNFMY2XJ0edu2GBllzUIBInhuq6hhtAFrn1KukS0ZZPSZELqXaS0HxdRuSq5nZmlUPeH7ItrQ+EWrZz5trUdWBti6QQRPADg48fNdNsi5tOakTGseClWeEUbdvcaB9fdRsVr5mODRqQR5lUpGbjRgrDDM7KtdpJm4cwNBGw1GnUk+i6X2ftzToMaRBE6eeyznY3DhQLWuY5xEhztPhnNu5p3JB0W0ygaN2W/jRvI5GHlzrEoBFIKU4pDl6J5QQQIQaEooGIKQ5OEJt6BCQhCARoASQgQjKJABQiS0FJVDtp8jf0j2Cl01GtW9xv6W+wUhgWd6KY6EZCDQjKXQhlyaeU88JlwVxBoRmREoFJWiIAghCetrZzzDR4ngPEpgSLUinSqVSYMENdoY01IC5d9lmLOqYw59R0uqU6zAdpiHDT/1XTcWrNDPhg6AR49T4rjOAv/xcUbUOzHudy7rtHfQlcua+NnXgrlR3quYcVm+0GKOpjSdlpcwdDhqCFT4rRY12Z8Q3XVec1Z6SlRWdk6VXP/kXBIYAXBgkkDmVpcY7RUQ1pDhBAI89pUPD7trmZ2mZnbVYPt3Naq2m0kQCdAYHIFUlxWh8rey3xXGKNxOWoDUaJa2RJ5ykWeIabQVQ9ncPpUtcoLyNXcfDorS5aAczfMJNXspSS0Tbm+JEKNTbOg1JICjPM7KxwSmS4RlkajMYH+0Y43JIjLOotkzDcH+FvWe8SSA4NESZ4bqyJUPEMWo0Na1Sm0c2va4A9RuE467pktaKjCXDM0BzSXDmAvUxwjBUjx5znPsdLkRKJEtTENGko0DsMptwS0IQA2ECl5UhyAEoIJLigBSCazIKR2WNoO439LfYKQ0Jm0+Rn6W+ykALBFsMI0AgmIQ4JpwUghIc1NOiiI9qTlUh4TJWqZm0Kt6Be4NHH6DiVbXNRtJhDdA3fm53MqNhrg0OdxJa3y3Psq3Hbv7kGd9fM/7Q9suK0VF9dEgkFcy7RO7+ZvzAn67rdVq3dKxOM0e8Sm4pqgTppnSPs17Ri4oCk4/eUwAROpGwKusftzVOTn6LhWF4lUtK7a9I6tOo4PbxaehC7rgmJU7yi2tTMg8OIPEHqF5k4OLPRhNSQzS7Lup0x/j3T6Z1MOJdTcT+Zu48iFksTqYpTe5pt2VZOj2kERtxW/xC7dTZoNY5Tsud33aGvndFMATA1qZo/ZRaR0wd9lTc0b9gzG3DJ2GZoJ8pR4fiFcEsrMLSPNW1liQdrlOY7lxcfSUnEG5tYSb+hSofZXGVV+OYn8JtIB2VznF+nJug+pPoisyS7Xh78Fke1d8XXTo2phtMf+urvqStcPplZz5ncaNu3C7S/bnc0MrO3e3QOPUc+qmYX2Ap06lOp8epmpmWgxlMfhmNFiezmOhpyv0BXS8CxY1WxE8mmcxHPTYdSvSVSVnmtyjplpU0JHEIgUKtUPG8OGx3joeYTFvcZpBEOGjhyPPwKozJIKEpAKCQxaNJlKBQINIcEpAoGMpt6dcm3oEIQQQQBZWju439LfYKU0qBaO7jf0t9gpjHLBo1Y5KNJlHKQgyUUpJcizIGBzUy5ikJ6ztPiOjgNSeipMVECvXNKkXkGMwI00OXf6SsliGJmoSwn5fYkkEdOHktx2tuqbaOUwGtII2jQGSSdhBMnquaXxDHZhrlEfqpnl1b+y1jsGqH6ru6s7ibZlXlWrI0PmFTXWp1ViZnK1Gd1a9iO07sPrnNJo1IFRvLk9o5j6hRbmnB0UC9oyJXPmx2jbFOjvpxmk9gcHBwcAQQQQQdiCqy6qU3HN3fouJYfjFaj3WPIb+U6tnmBw8lO/8A6GufxALznFpnoxmqOm3Nai3bL46BZzGceY3uNMk/RZJ1evU1znyMD0T1phxmX/yldFPZdWd4AHOn5QXnyE/ssbWOcl3Ekk+J1K6BguCh7Kg1hzSD4FYrELI0Kxpu2n/RWuGSejHLFla0lp5Eaq4bilwWZactbxLZ8yVBvKEH28FJwym4d5s6cW6nzG8LqgmnRzSaqzV2tu9lJtahWe4x3iHO1PHMDsVY4f2jcSHOcXFphwIaHZJ123jfyVFht6aZLmkDnpoejgnryzFX76ho8aupjfxbz8F1Ucb7OlUnhwBBkESD0S1n+xeICrRy8WHbiJ4eC0Qak0AUIwEYCNIdAlFmSXJKYhTk1USklyB0NoIIIFRJtflb+kewU2mVEtR3W/pHsFJWDZoxzMizpsokIQpz0QciISQFVICXbsLiANyrl9RtJmUceW5cm7C2+GyT8x+nRUnaa/ygsb85GpH4Qd9eGm54JJJsvpGN7bXvxy6i13d/EQdDBMR/xBmBxMnksnh2IOI+BV0ez5SeIHA84Gh6eBWhFuS/p79T0VB2pojdpylhku/M7gR1WrVERduhdC4LZYdBMAad0kTl8OSaLZKr6N+Kokjvj5htmA5deI9EZuzGp32PMfympWKUWhdw3VRy0FE66CQ66aNk20JWV13bQdFG7zSr91sXySA2BPKBvLz15KvuK9MDLGZ3A6gN8AN/Nc08cTqxzZLwO8h4BGi6DQw2nVZIDZ+v0XLmME66dRwPkre0xu8oiKNclsfiax8c9SJXK/HfwdK8hLTOn4LTZRa8GGgalxPDrK5T2rv2VrhzqZlo0nnrw6KNiOJXFc/e1XO6bN/+RAUICFUMfEmeTkTj95TDeLRp16dVEsLp1N4I4FKp1IRXlP8AGI6gbTzXQ38r4MEvhm4tqdK4aHgQ/mND581FqUX0agPXRw0B6HkVWdlbk6t81pgA4Q4SF0xdqzllGnQi0vBQuW1Bpn+YDQPHHT8wXQGEESNQdlhcXwunVsi+m/7+i4uc2dSye64eWnkrnsDi3+RbCfmpnI73B9FDl8DUdWaUNSSjzJJKBiXBISiUhBLAiIRlESqCxBagiJQQInWvyN/S32T4Ua0d3G/pb7BSmlc1GjElqLKnEExCMissLsx/5HcPlHM8/dR7S3znkBqT+yqu3vaJtr8Om52Rrw7vcJbHcJ4fMPIFNMpIlY12ndSc5otaj2gx8RjqJkRq5rM0wNR5bLIV+0tCtLWEydS14Id0mYMDfkpNpVFWHB2cHUEEQepj2VR2mq2tKnne2SZy/neeBB/C36rVRS2F3oXe4mymwk6aGTxPh/YWNql9y+T3WA91v7nmotOvWrSwSGOO0kwOUndaC3pZU0+RL9H9KTH7UMLXNEeChsqZwTx3I016hXmN08wb5qgfRIMjdTKNbRUZWqZtsB+y+8u7YXLH0mNcM1Nry8Go2NHSB3QeE9NlkxYPp1HNqjK+mSHAwQwt0M8zOwWzwH7UK9tbNt4aQxuVpJgtAENG2sCBPRYy8r1bh7nn8bi9x5uP7DYLNW2ashX17PcZo2Z8TxJ6qERsVbii2no9rS18AmJfTE/M08DzHEKL/hltX4Z4HyI3kdCFE4tuiotJaJt1ZkNzfhn0J58kVAQJn1/76q/w2qAcrhIMiOka/VQ8Vwf4cvb8h14nL0I4rpUUjn53plW6mHch/HhzRDD51B06/wAKQxo4a6H131H93SXSeM/t/fNDgmO2iK63A15eCjVmEA6aK0yjnHnr6KLWZp+6iUNaKjL7D7NVw2tB/EI8+C2YXOiS10jcGVu7G5zsa7mPrxSwS1Qsy3ZN034wR5HfxT/YSh8KtcNE5Xhjx6uB91FFTvDrp6q3wB2WoW8wQPJaSRmmakFNvQCJxSGFKIoiUaZFjbkUpT024pgHKCblBAEu0qd1v6W+wUtr1WWju63wHspjSsmi7JOZGCdkwCpFm7vSeCgY5iN2KTRT11mXAkd6OY1WQxhja8h7Q4O5gFaHERnkHZZ+6aWmFSL/AIZPEOz4ZrbPfSdrIa94BHqqGtaXFV33tQvyCNdwP6FsrmtuFCc9pER/ea04pkc5Ii2VuA0QIUnKkh2UQduf8pD6mkrQzI94cxHQEqC+jDv7xUrNMnqPDTVQzdEVMp1adChioVUsxOYDVSqVMERCeDeHp1CLJBQkhNsr7ygScumsST46a/3godgS4yfwDID03Anpt5qzxiGsa8/mjxBH/SiWgAZOgzSdOXqs2rZvGVRJlpVIPh4bkydlpbNzajYOvMLL0XECY31Vlht3B5LQysiYrhXwXSP/ABk6a/KTwP8AKg1JMkGPX6eS3TqbajIIBB0MrKYlY/Bdt3SZBMxyIPggdlX8PSf49v7smaxjaDvrv5p95nU8P7McfJNVYBOv8JPouJV3I4rQ9mLiW5eWvqqS6SsFr5XwuaOpm0tw/hs3P1CmU7gteD+UgyP7p4KsYSVNvB3iYjuzEEfiH8rqZzm8zSJHHVJKgdnaxdbUydTlg+RhWChg+hCMIFGhCSElMvTjim3pgxtBBGgQ5aN7rfAewUxiYtWd1v6R7KWxqyctF1TDaETiQR6p5rUyXS5Qhh1BoqTESrqq/RUGJvVoozl8NSqqpUhWt9UmVR13K0Qxx1xOih3l2WCB8v1HRR6teDuotWtm31TcqBImUsSAGszB25nVFejvSOQPnsqeqIU2hXzME7jTyKiOS9MuWNJWXWF3OcRxCsmtWSt6xpukK3uMZaGafMVopqtmbh9DvaNs02j/AJD91tOyH2bNv7f4xuDTALmNa1rTJZoS6eE8By3105rSxF9VrqbzM6g8QRrurvsz29urIObSeA1+rmvBc3NoM41EGAJ14LGUr2jaEa0xvE7apa1qlvVjNTcWkiYMbEdCCD5qMKusgp66vhcPfUqVM73uL3OJgknkNv8ASNtEcPYO9S1bRkYyjsuMExP8LirPELZr2kHUH6ciFlKUtPdAJ6H9lf2GIyMrwQdtf5VEmZvaJpuLTw2I4jmoZZpvH9+mi1mNWnxG93cbfwVkHMMkbwTO/wBVMjSIzXc3YDf6KHRdD5Utzf7wUCpoVzZNOzoj0zY2taWgyri5OYtgzLKoHLQFw146tWawOsCCDwghXznHPR4a1N44tqLpTtWcj06L/sZcTTNOdmsd/wDQJV+4rJdm6NSjcZXjKH0ob1+Fka70MrUuKXYxcoi5N5kWZMBRKbJRkpBQKwI0SCAssLb5G+DfZSGo0FzlsWolPdBBUgQmvt6rO4mUEE0W+jO3Oyp7pEgtEZMprg6pFJBBZv3Gq6G7lFa7FGgsl7zR+wfq7piuggrl0zOPY9h+6N7RG3NGgl/kt+5kB+hU2xqO/MfUoIJQ7RUui/sDMzrqd9eLVcYcPvQ3h3tOHHgggupHIyUfkWQxkfeOHn5wNUEEpdFQ7IR+UdZnr4qtr7oILny9HTAucAPfHiPcLVVR9/R/U7/81EEFtD8ZzP8AIXOcmphpJJLrVxcTu4kiSTxOgV+5BBNdCkNoIIJkgKSUEEAGggggR//Z" class="people-img header-user1">
                            @endif
                        </a>
                        <ul class="dropdown-menu userprofile_down" role="menu" style="margin-left: -116px;margin-top: -21px;">
                            <li>
                                <center><a href="#" target="_blank">
                                    <i class="fa fa-user" aria-hidden="true"></i> My Profile
                                </a><br>
                                <a href="{{ route('client.logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('client.logout') }}" method="GET"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                </center>
                            </li>
                        </ul>
                    </li>
                 @else
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/client/register')}}">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/client/login')}}">Log In</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<header class="masthead text-center text-white">
    <div class="masthead-content">
        <div class="container">
            <h1 class="masthead-heading mb-0">Role Based Project</h1>
            <h2 class="masthead-subheading mb-0">Will Rock Your Socks Off</h2>
            <a href="#" class="btn btn-primary btn-xl rounded-pill mt-5">Learn More</a>
        </div>
    </div>
    <div class="bg-circle-1 bg-circle"></div>
    <div class="bg-circle-2 bg-circle"></div>
    <div class="bg-circle-3 bg-circle"></div>
    <div class="bg-circle-4 bg-circle"></div>
</header>

