@extends('agent.agentlayouts.master')
@section('css')
    <link href="{{asset('css/client-mgmt.css')}}" rel="stylesheet">
@endsection
@section('usermaster')
    <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">
            </div>
        </div>
    </header>
    <!-- About Section -->
    <section id="about" class="about-section text-center register-header client-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h4 class="text-white mb-4 register-title">CLIENT MANAGEMENT</h4>
                </div>
            </div>
        </div>
    </section>
    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light dashboard-tabs client-tabs">
        <div class="container-fluid">

            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#summary-of-clients" data-toggle="tab" class="active show">
                                SUMMARY OF CLIENTS </a>
                        </li>
                        <li>
                            <a href="#prospective-clients" data-toggle="tab">
                                PROSPECTIVE CLIENTS </a>
                        </li>
                        <li>
                            <a href="#investment-progress" data-toggle="tab">
                                INVESTMENT PROCESS IN PROGRESS </a>
                        </li>
                        <li>
                            <a href="#manage-clients" data-toggle="tab">
                                MANAGE CLIENTS </a>
                        </li>
                    </ul>

                    <!-- search Section starts-->
                    <section id="" class="search-section text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 mx-auto">


                                    <div class="social d-flex justify-content-center">
                                        <div class="custom-search-input" id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control input-lg search-field" placeholder="SEARCH" />
                          <span class="input-group-btn">
                              <button class="btn btn-info btn-lg" type="button">
                                  <i class="fas fa-search"></i>
                              </button>
                          </span>
                                            </div>
                                        </div>
                                        <div class="small-cases-btn" id="">
                                            <div class="input-group col-md-12">
                          <span class="input-group-btn">
                              <button class="btn btn-info btn-lg" type="button">
                                  Sort By
                              </button>
                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="tab-content">
                        <div class="tab-pane active" id="summary-of-clients">
                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>vijay bhandari</h4>
                                                <h6><span class="status-red"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567890</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab-1">
                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Call</h4>
                                    </div>
                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Whatsapp</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>john appleseed</h4>
                                                <h6><span class="status-green"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567891</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab-1">
                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Client Profile</h4>
                                    </div>
                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Redeem Investment</h4>
                                    </div>
                                    <div class="period-returns-risks" id="">
                                        <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                                    </div>
                                </div>
                            </div>
                            <div class="search-funds-tab-outer investment-progress-tab">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>Rushabh Ashar</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Client ID</h6>
                                                <h4>1234567892</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Exp. Completion Date</h6>
                                                <h4>12th December, 2018</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Progress</h6>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h4 class="">Steps Completed 5/8</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <div class="period-returns-risks" id="">
                                                    <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>Nirmeet Sanghani</h4>
                                                <h6><span class="status-green"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567893</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab-1">
                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Call</h4>
                                    </div>
                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Whatsapp</h4>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane prospective-clients" id="prospective-clients">
                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details border-radius-top" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>vijay bhandari</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567890</h4>
                                            </div>
                                        </div>
                                        <div class="social d-flex justify-content-center prospective-clients-details">
                                            <div class="company-details-1 fund-title">
                                                <h6>Email ID</h6>
                                                <h4>vijaybhandari@gmail.com</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Phone Number</h6>
                                                <h4>+91 7894561230</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>vijay bhandari</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567890</h4>
                                            </div>
                                        </div>
                                        <div class="social d-flex justify-content-center prospective-clients-details">
                                            <div class="company-details-1 fund-title">
                                                <h6>Email ID</h6>
                                                <h4>vijaybhandari@gmail.com</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Phone Number</h6>
                                                <h4>+91 7894561230</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>vijay bhandari</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567890</h4>
                                            </div>
                                        </div>
                                        <div class="social d-flex justify-content-center prospective-clients-details">
                                            <div class="company-details-1 fund-title">
                                                <h6>Email ID</h6>
                                                <h4>vijaybhandari@gmail.com</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Phone Number</h6>
                                                <h4>+91 7894561230</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-bottom">
                                    <div class="company-details border-radius-bottom" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>vijay bhandari</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567890</h4>
                                            </div>
                                        </div>
                                        <div class="social d-flex justify-content-center prospective-clients-details">
                                            <div class="company-details-1 fund-title">
                                                <h6>Email ID</h6>
                                                <h4>vijaybhandari@gmail.com</h4>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Phone Number</h6>
                                                <h4>+91 7894561230</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add-client">
                                <a href="#" class="" data-toggle="modal" data-target="#add-client-modal"><img src="{{asset('images/add-client.png')}}"></a>
                            </div>
                        </div>
                        <div class="tab-pane" id="investment-progress">
                            <div class="search-funds-tab-outer investment-progress-tab">
                                <div class="social d-flex justify-content-center search-funds-tab">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>Rushabh Ashar</h4>
                                            </div>

                                            <div class="company-details-1 fund-title">
                                                <h6>Client ID</h6>
                                                <h4>1234567892</h4>
                                            </div>

                                            <div class="company-details-1 fund-title">
                                                <h6>Exp. Completion Date</h6>
                                                <h4>12th December, 2018</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Progress</h6>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h4 class="">Steps Completed 5/8</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <div class="period-returns-risks" id="">
                                                    <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-funds-tab-outer investment-progress-tab">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>Rushabh Ashar</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Client ID</h6>
                                                <h4>1234567892</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Exp. Completion Date</h6>
                                                <h4>12th December, 2018</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Progress</h6>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h4 class="">Steps Completed 5/8</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <div class="period-returns-risks" id="">
                                                    <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-funds-tab-outer investment-progress-tab">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>Rushabh Ashar</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Client ID</h6>
                                                <h4>1234567892</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Exp. Completion Date</h6>
                                                <h4>12th December, 2018</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Progress</h6>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h4 class="">Steps Completed 5/8</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <div class="period-returns-risks" id="">
                                                    <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-funds-tab-outer investment-progress-tab">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Client Name</h6>
                                                <h4>Rushabh Ashar</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Client ID</h6>
                                                <h4>1234567892</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h6>Exp. Completion Date</h6>
                                                <h4>12th December, 2018</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">
                                    <div class="company-details client-mgmt-progress" id="">
                                        <div class="social d-flex justify-content-center m-d-flex">
                                            <div class="company-details-1 fund-title">
                                                <h6>Progress</h6>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <h4 class="">Steps Completed 5/8</h4>
                                            </div>
                                            <div class="company-details-1 fund-title">
                                                <div class="period-returns-risks" id="">
                                                    <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="manage-clients">

                            <!-----------------manage-clients inner starts here------------------>

                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>john appleseed</h4>
                                                <h6><span class="status-green"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567891</h4>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Client Profile</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Redeem Investment</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                                    </div>

                                </div>

                            </div>

                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>john appleseed</h4>
                                                <h6><span class="status-red"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567891</h4>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Client Profile</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Redeem Investment</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                                    </div>

                                </div>

                            </div>

                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>john appleseed</h4>
                                                <h6><span class="status-green"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567891</h4>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Client Profile</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Redeem Investment</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                                    </div>

                                </div>

                            </div>

                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>john appleseed</h4>
                                                <h6><span class="status-green"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567891</h4>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Client Profile</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Redeem Investment</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                                    </div>

                                </div>

                            </div>

                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>john appleseed</h4>
                                                <h6><span class="status-red"></span>Active</h6>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h6>Client ID</h6>
                                                <h4>1234567891</h4>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Client Profile</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">Redeem Investment</h4>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                                    </div>

                                </div>

                            </div>

                            <!-----------------manage-clients inner ends here------------------>

                            <div class="add-client">
                                <a href="#" class="" data-toggle="modal" data-target="#add-client-modal"><img src="{{asset('images/add-client.png')}}"></a>
                            </div>

                        </div>
                        <!-----------------manage-clients ends here------------------>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade client-mgmt-modal" id="viewdetails-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Investment Amount</h4>
                </div>
                <div class="modal-body">
                    <div class="d-flex">
                        <div class="piechart-circle">
                            <h6>PM Securities | Bull rider fund (AIF)</h6>
                        </div>

                        <div class="piechart-elements">
                            <h6>2500000</h6>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="piechart-circle">
                            <h6>Mosl | Growth fund (PMS)</h6>
                        </div>

                        <div class="piechart-elements">
                            <h6>10000000</h6>
                        </div>
                    </div>


                    <div class="d-flex">
                        <div class="piechart-circle">
                            <h6>OLA (Private Equity) 1000 shares @ Rs.1234/share</h6>
                        </div>

                        <div class="piechart-elements">
                            <h6>1000000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade client-mgmt-modal" id="add-client-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Client</h4>
                </div>
                <div class="modal-body form-inline">
                    <form class="add-client-form">
                        <div class="form-group d-flex">
                            <label for="exampleFormControlInput1">Client Name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="">
                        </div>
                        <div class="form-group d-flex">
                            <label for="exampleFormControlSelect1">Email ID</label>
                            <input type="email" class="form-control" id="exampleFormControlInput2" placeholder="">
                        </div>
                        <div class="form-group d-flex">
                            <label for="exampleFormControlSelect2">Phone No.</label>
                            <input type="number" class="form-control" id="exampleFormControlInput31" placeholder="">
                        </div>
                        <div class="period-returns-risks save-client" id="">
                            <button class="">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>


    <!-- viewprogress Modal -->
    <div class="modal fade client-mgmt-modal" id="viewprogress-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Steps completed 5/8</h4>
                </div>
                <div class="modal-body">

                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Investment request raised with fundsmap</h6>
                                <img class="status-icon" src="{{asset('images/checked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Documentation done with client</h6>
                                <img class="status-icon" src="{{asset('images/checked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Document verification in progress</h6>
                                <img class="status-icon" src="{{asset('images/checked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Investment request raised with fundsmap</h6>
                                <img class="status-icon" src="{{asset('images/checked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Investment request raised with fundsmap</h6>
                                <img class="status-icon" src="{{asset('images/checked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/unchecked.png')}}">
                                <h6>Investment request raised with fundsmap</h6>
                                <img class="status-icon" src="{{asset('images/unchecked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Investment request raised with fundsmap</h6>
                                <img class="status-icon" src="{{asset('images/unchecked.png')}}">
                            </div>
                        </div>
                        <div class="item">
                            <div class="steps-completed-block">
                                <img class="step-icon" src="{{asset('images/user.png')}}">
                                <h6>Investment request raised with fundsmap</h6>
                                <img class="status-icon" src="{{asset('images/unchecked.png')}}">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection