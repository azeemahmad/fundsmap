@extends('agent.agentlayouts.master')
@section('css')
    <link href="{{asset('css/client-mgmt.css')}}" rel="stylesheet">
    <link href="{{asset('css/relationship-mgr.css')}}" rel="stylesheet">
@endsection
@section('usermaster')
    <header class="masthead">
      <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
          <!-- <h1 class="mx-auto my-0 text-uppercase">Grayscale</h1>
          <h2 class="text-white-50 mx-auto mt-2 mb-5">A free, responsive, one page Bootstrap theme created by Start Bootstrap.</h2>
          <a href="#about" class="btn btn-primary js-scroll-trigger">Get Started</a> -->

          <!-- <div class="fundsmap-video">
            <img src="images/video.jpg">
          </div> -->
        </div>
      </div>
    </header>


    <!-- About Section -->
    <section id="about" class="about-section text-center register-header client-header relationship-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h4 class="text-white mb-4 register-title">Relationship MANAGER DASHBOARD</h4>
            <!-- <div class="register-subtitle">
              <h4 class="text-white">Please enter your</h4>text-white-50
              <h3 class="text-white">Phone Number</h3>
            </div> -->
          </div>
        </div>
        <!-- <img src="img/ipad.png" class="img-fluid" alt=""> -->
      </div>
    </section>



    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light dashboard-tabs client-tabs relationship-tabs">
      <div class="container-fluid">

      <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">
            <li class="active">
              <a href="#mapped-associates" data-toggle="tab" class="active show">
              MAPPED PROSPECTIVE ASSOCIATES </a>
            </li>
            <li>
              <a href="#total-earnings" data-toggle="tab">
              TOTAL EARNINGS </a>
            </li>
          </ul>

          <!-- search Section starts-->
          <section id="" class="search-section text-center">
            <div class="container">
              <div class="row">
                <div class="col-sm-12 mx-auto">


                  <div class="social d-flex justify-content-center">
                    <div class="custom-search-input" id="custom-search-input">
                      <div class="input-group col-md-12">
                          <input type="text" class="form-control input-lg search-field" placeholder="SEARCH" />
                          <span class="input-group-btn">
                              <button class="btn btn-info btn-lg" type="button">
                                  <i class="fas fa-search"></i>
                                  <!-- <i class="glyphicon glyphicon-search"></i> -->
                              </button>
                          </span>
                      </div>
                    </div>

                    <!-- <div class="filter" id="">
                      <i class="fas fa-filter"></i>
                    </div> -->

                    <div class="small-cases-btn" id="">
                      <div class="input-group col-md-12">
                          <span class="input-group-btn">
                              <button class="btn btn-info btn-lg" type="button">
                                  Sort By
                              </button>
                          </span>
                      </div>
                    </div>

                  </div>



                </div>
              </div>
              <!-- <img src="img/ipad.png" class="img-fluid" alt=""> -->
            </div>
          </section>
        <!-- search Section ends-->


          <div class="tab-content">
            <div class="tab-pane active" id="mapped-associates">

    <!-----------------search-funds started here------------------>
        <div class="search-funds-tab-outer">
            <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                <div class="company-details" id="">
                  <div class="social d-flex justify-content-center">
                    <div class="company-details-1 fund-title">
                      <h4>vijay bhandari</h4>
                      <h6><span class="status-green"></span>Active</h6>
                    </div>
                    <!-- <div class="company-details-2 star">
                      <i class="fas fa-star"></i>
                    </div> -->
                    <div class="company-details-3 rank">
                      <h6>Client ID</h6>
                      <h4>1234567890</h4>
                    </div>
                  </div>

                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="contact-label">Contact</h4>
                </div>

                <div class="period-returns-risks d-flex relation-details" id="">
                  <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                  <h4 class="icons"><i class="fas fa-phone"></i></h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">Update profile</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                    <a href="#" class="" data-toggle="modal" data-target="#update-modal"><h4 class="update-profile">Update</h4></a>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">date when mapped</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                  <h4 class="mapped-date">15/12/2018</h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

              </div>

        </div>


        <div class="search-funds-tab-outer">
            <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                <div class="company-details" id="">
                  <div class="social d-flex justify-content-center">
                    <div class="company-details-1 fund-title">
                      <h4>vijay bhandari</h4>
                      <h6><span class="status-green"></span>Active</h6>
                    </div>
                    <!-- <div class="company-details-2 star">
                      <i class="fas fa-star"></i>
                    </div> -->
                    <div class="company-details-3 rank">
                      <h6>Client ID</h6>
                      <h4>1234567890</h4>
                    </div>
                  </div>

                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="contact-label">Contact</h4>
                </div>

                <div class="period-returns-risks d-flex relation-details" id="">
                  <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                  <h4 class="icons"><i class="fas fa-phone"></i></h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">Update profile</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                    <a href="#" class="" data-toggle="modal" data-target="#update-modal"><h4 class="update-profile">Update</h4></a>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">date when mapped</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                  <h4 class="mapped-date">15/12/2018</h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

              </div>

        </div>


        <div class="search-funds-tab-outer">
            <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                <div class="company-details" id="">
                  <div class="social d-flex justify-content-center">
                    <div class="company-details-1 fund-title">
                      <h4>vijay bhandari</h4>
                      <h6><span class="status-green"></span>Active</h6>
                    </div>
                    <!-- <div class="company-details-2 star">
                      <i class="fas fa-star"></i>
                    </div> -->
                    <div class="company-details-3 rank">
                      <h6>Client ID</h6>
                      <h4>1234567890</h4>
                    </div>
                  </div>

                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="contact-label">Contact</h4>
                </div>

                <div class="period-returns-risks d-flex relation-details" id="">
                  <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                  <h4 class="icons"><i class="fas fa-phone"></i></h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">Update profile</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                    <a href="#" class="" data-toggle="modal" data-target="#update-modal"><h4 class="update-profile">Update</h4></a>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">date when mapped</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                  <h4 class="mapped-date">15/12/2018</h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

              </div>

        </div>




        <div class="search-funds-tab-outer">
            <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                <div class="company-details" id="">
                  <div class="social d-flex justify-content-center">
                    <div class="company-details-1 fund-title">
                      <h4>vijay bhandari</h4>
                      <h6><span class="status-green"></span>Active</h6>
                    </div>
                    <!-- <div class="company-details-2 star">
                      <i class="fas fa-star"></i>
                    </div> -->
                    <div class="company-details-3 rank">
                      <h6>Client ID</h6>
                      <h4>1234567890</h4>
                    </div>
                  </div>

                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="contact-label">Contact</h4>
                </div>

                <div class="period-returns-risks d-flex relation-details" id="">
                  <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                  <h4 class="icons"><i class="fas fa-phone"></i></h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">Update profile</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                    <a href="#" class="" data-toggle="modal" data-target="#update-modal"><h4 class="update-profile">Update</h4></a>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1">

                <div class="period-returns-risks relation-label" id="">
                  <h4 class="">date when mapped</h4>
                </div>

                <div class="period-returns-risks relation-details" id="">
                  <h4 class="mapped-date">15/12/2018</h4>
                </div>

              </div>

              <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

              </div>

        </div>




        <!--search-funds-tab-outer-ends here-->

              <!-----------------search-funds ends here------------------>


            </div>


<!-----------------prospective-clients starts here------------------>

            <div class="tab-pane prospective-clients" id="total-earnings">

<!-----------------prospective-clients inner starts here------------------>

              <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>vijay bhandari</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567890</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="contact-label">Contact</h4>
                    </div>

                    <div class="period-returns-risks d-flex relation-details" id="">
                      <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                      <h4 class="icons"><i class="fas fa-phone"></i></h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r aum-details">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="">AUM</h4>
                    </div>

                    <div class="period-returns-risks relation-details" id="">
                        <h4>51 Cr.</h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

                  </div>

            </div>

            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>vijay bhandari</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567890</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="contact-label">Contact</h4>
                    </div>

                    <div class="period-returns-risks d-flex relation-details" id="">
                      <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                      <h4 class="icons"><i class="fas fa-phone"></i></h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r aum-details">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="">AUM</h4>
                    </div>

                    <div class="period-returns-risks relation-details" id="">
                        <h4>51 Cr.</h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

                  </div>

            </div>


            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>vijay bhandari</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567890</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="contact-label">Contact</h4>
                    </div>

                    <div class="period-returns-risks d-flex relation-details" id="">
                      <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                      <h4 class="icons"><i class="fas fa-phone"></i></h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r aum-details">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="">AUM</h4>
                    </div>

                    <div class="period-returns-risks relation-details" id="">
                        <h4>51 Cr.</h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

                  </div>

            </div>


            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>vijay bhandari</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567890</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="contact-label">Contact</h4>
                    </div>

                    <div class="period-returns-risks d-flex relation-details" id="">
                      <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                      <h4 class="icons"><i class="fas fa-phone"></i></h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r aum-details">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="">AUM</h4>
                    </div>

                    <div class="period-returns-risks relation-details" id="">
                        <h4>51 Cr.</h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

                  </div>

            </div>
            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>vijay bhandari</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567890</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="contact-label">Contact</h4>
                    </div>

                    <div class="period-returns-risks d-flex relation-details" id="">
                      <h4 class="icons"><i class="fab fa-whatsapp"></i></h4>
                      <h4 class="icons"><i class="fas fa-phone"></i></h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 no-border-r aum-details">

                    <div class="period-returns-risks relation-label" id="">
                      <h4 class="">AUM</h4>
                    </div>

                    <div class="period-returns-risks relation-details" id="">
                        <h4>51 Cr.</h4>
                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1 bg-blue">

                  </div>

            </div>

<!-----------------prospective-clients inner ends here------------------>


        <!-- <div class="add-client">
          <a href="#" class="" data-toggle="modal" data-target="#add-client-modal"><img src="images/add-client.png"></a>
        </div> -->

      </div>

<!-----------------prospective-clients ends here------------------>

<!-----------------investment-progress starts here------------------>

      <div class="tab-pane" id="investment-progress">

<!-----------------investment-progress inner starts here------------------>

              <div class="search-funds-tab-outer investment-progress-tab">
                <div class="social d-flex justify-content-center search-funds-tab">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Client Name</h6>
                          <h4>Rushabh Ashar</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Client ID</h6>
                          <h4>1234567892</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Exp. Completion Date</h6>
                          <h4>12th December, 2018</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Progress</h6>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h4 class="">Steps Completed 5/8</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <div class="period-returns-risks" id="">
                            <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

            </div>


            <div class="search-funds-tab-outer investment-progress-tab">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Client Name</h6>
                          <h4>Rushabh Ashar</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Client ID</h6>
                          <h4>1234567892</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Exp. Completion Date</h6>
                          <h4>12th December, 2018</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Progress</h6>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h4 class="">Steps Completed 5/8</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <div class="period-returns-risks" id="">
                            <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

            </div>




            <div class="search-funds-tab-outer investment-progress-tab">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Client Name</h6>
                          <h4>Rushabh Ashar</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Client ID</h6>
                          <h4>1234567892</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Exp. Completion Date</h6>
                          <h4>12th December, 2018</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Progress</h6>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h4 class="">Steps Completed 5/8</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <div class="period-returns-risks" id="">
                            <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

            </div>




            <div class="search-funds-tab-outer investment-progress-tab">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Client Name</h6>
                          <h4>Rushabh Ashar</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Client ID</h6>
                          <h4>1234567892</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h6>Exp. Completion Date</h6>
                          <h4>12th December, 2018</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab border-radius-top progress-status">

                    <div class="company-details client-mgmt-progress" id="">
                      <div class="social d-flex justify-content-center m-d-flex">
                        <div class="company-details-1 fund-title">
                          <h6>Progress</h6>
                        </div>

                        <div class="company-details-1 fund-title">
                          <h4 class="">Steps Completed 5/8</h4>
                        </div>

                        <div class="company-details-1 fund-title">
                          <div class="period-returns-risks" id="">
                            <a href="#" class="" data-toggle="modal" data-target="#viewprogress-modal"><h4 class="viewprogress-status">View</h4></a>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

            </div>
<!-----------------investment-progress inner ends here------------------>



      </div>

<!-----------------investment-progress ends here------------------>


<!-----------------manage-clients starts here------------------>

            <div class="tab-pane" id="manage-clients">

<!-----------------manage-clients inner starts here------------------>

              <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>john appleseed</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567891</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1">

                    <div class="period-returns-risks" id="">
                      <h4 class="">Client Profile</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <h4 class="">Redeem Investment</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                    </div>

                  </div>

            </div>

            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>john appleseed</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567891</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1">

                    <div class="period-returns-risks" id="">
                      <h4 class="">Client Profile</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <h4 class="">Redeem Investment</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                    </div>

                  </div>

            </div>

            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>john appleseed</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567891</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1">

                    <div class="period-returns-risks" id="">
                      <h4 class="">Client Profile</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <h4 class="">Redeem Investment</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                    </div>

                  </div>

            </div>

            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>john appleseed</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567891</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1">

                    <div class="period-returns-risks" id="">
                      <h4 class="">Client Profile</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <h4 class="">Redeem Investment</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                    </div>

                  </div>

            </div>

            <div class="search-funds-tab-outer">
                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                    <div class="company-details" id="">
                      <div class="social d-flex justify-content-center">
                        <div class="company-details-1 fund-title">
                          <h4>john appleseed</h4>
                          <h6><span class="status-green"></span>Active</h6>
                        </div>
                        <!-- <div class="company-details-2 star">
                          <i class="fas fa-star"></i>
                        </div> -->
                        <div class="company-details-3 rank">
                          <h6>Client ID</h6>
                          <h4>1234567891</h4>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="social d-flex justify-content-center search-funds-tab-1">

                    <div class="period-returns-risks" id="">
                      <h4 class="">Client Profile</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <h4 class="">Redeem Investment</h4>
                    </div>

                    <div class="period-returns-risks" id="">
                      <a href="#" class="" data-toggle="modal" data-target="#viewdetails-modal"><h4 class="">View</h4></a>
                    </div>

                  </div>

            </div>

<!-----------------manage-clients inner ends here------------------>

        <div class="add-client">
          <a href="#" class="" data-toggle="modal" data-target="#add-client-modal"><img src="{{asset('images/add-client.png')}}"></a>
        </div>

        </div>
<!-----------------manage-clients ends here------------------>

          </div>
        </div>
      </div>





      </div>
    </section>

    <!-- <section class="projects-section bg-light your-role partner-role">

        <div class="row justify-content-center no-gutters mb-5 mb-lg-0 your-role">
          <div class="col-lg-6 left-block">
            <div class="bg-black text-center h-100 project">
              <div class="d-flex h-100">
                <div class="project-text w-100 my-auto text-center text-lg-left">
                  <h4 class="text-white text-left">Your Role<br>As a partner</h4>
                  <h4 class="mb-0 text-white-50 sub-title text-left">use fundmap's platform to identify the right pms/aif/pe.</h4>
                  <p class="text-left">Our platform has exhaustive detailing, comparisons & analysis.</p>
                  <h4 class="mb-0 text-white-50 sub-title text-left">share summarized documents with your clients.</h4>
                  <p class="text-left">Created by FUNDS MAP to explain your clients about thePMS/AIF/PE that you find suitable for them.</p>
                  <h4 class="mb-0 text-white-50 sub-title text-left">click invest button on your fundsmap portal.</h4>
                  <p class="text-left">& let funds maptake care of the rest.</p>
                  <h4 class="mb-0 text-white-50 sub-title text-left">receive your remuneration.</h4>
                  <hr class="d-none d-lg-block mb-0 ml-0">
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 right-block">
            <img class="img-fluid" src="images/your-role-1.png" alt="">
          </div>

        </div>
    </section> -->



    <!-- <section class="projects-section bg-light your-role funds-map-role">

        <div class="row justify-content-center no-gutters ">
          <div class="col-lg-6 right-block">
            <img class="img-fluid" src="images/your-role-2.png" alt="">
          </div>
          <div class="col-lg-6 order-lg-first left-block">
            <div class="bg-black text-center h-100 project">
              <div class="d-flex h-100">
                <div class="project-text w-100 my-auto text-center text-lg-right">
                  <h4 class="text-white text-right">Funds map's role</h4>
                  <h4 class="mb-0 text-white-50 sub-title text-right">detailed analysis & data of all pms/aif/pe's.</h4>
                  <h4 class="mb-0 text-white-50 sub-title text-right">manage processing & paper-work on your behalf.</h4>
                  <p class="text-right">While you focus on growing your business.</p>
                  <h4 class="mb-0 text-white-50 sub-title text-right">platform for you to manage your client relationships & business.</h4>
                  <p class="text-right">& let funds map take care of the rest.</p>
                  <h4 class="mb-0 text-white-50 sub-title text-right">support from experts on all your clients queries & concerns.</h4>
                  <hr class="d-none d-lg-block mb-0 mr-0">
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>



    <section class="projects-section bg-light">
      <div class="whatsapp">
        <p class="whatsapp-button"><i class="fab fa-whatsapp"></i><span class="whatsapp-text">WHATS APP <br>HELP DESK</span></p>
      </div>
    </section> -->

    <!-- Signup Section -->
    <!-- section id="signup" class="signup-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-lg-8 mx-auto text-center">

            <i class="far fa-paper-plane fa-2x mb-2 text-white"></i>
            <h2 class="text-white mb-5">Subscribe to receive updates!</h2>

            <form class="form-inline d-flex">
              <input type="email" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" id="inputEmail" placeholder="Enter email address...">
              <button type="submit" class="btn btn-primary mx-auto">Subscribe</button>
            </form>

          </div>
        </div>
      </div>
    </section> -->

    <!-- Contact Section -->
    <!-- <section class="contact-section bg-black">
      <div class="container">

        <div class="row">

          <div class="col-md-4 mb-3 mb-md-0">
            <div class="card py-4 h-100">
              <div class="card-body text-center">
                <i class="fas fa-map-marked-alt text-primary mb-2"></i>
                <h4 class="text-uppercase m-0">Address</h4>
                <hr class="my-4">
                <div class="small text-black-50">4923 Market Street, Orlando FL</div>
              </div>
            </div>
          </div>

          <div class="col-md-4 mb-3 mb-md-0">
            <div class="card py-4 h-100">
              <div class="card-body text-center">
                <i class="fas fa-envelope text-primary mb-2"></i>
                <h4 class="text-uppercase m-0">Email</h4>
                <hr class="my-4">
                <div class="small text-black-50">
                  <a href="#">hello@yourdomain.com</a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 mb-3 mb-md-0">
            <div class="card py-4 h-100">
              <div class="card-body text-center">
                <i class="fas fa-mobile-alt text-primary mb-2"></i>
                <h4 class="text-uppercase m-0">Phone</h4>
                <hr class="my-4">
                <div class="small text-black-50">+1 (555) 902-8832</div>
              </div>
            </div>
          </div>
        </div>

        <div class="social d-flex justify-content-center">
          <a href="#" class="mx-2">
            <i class="fab fa-twitter"></i>
          </a>
          <a href="#" class="mx-2">
            <i class="fab fa-facebook-f"></i>
          </a>
          <a href="#" class="mx-2">
            <i class="fab fa-github"></i>
          </a>
        </div>

      </div>
    </section> -->

    <!-- Footer -->
    <!-- <footer class="bg-black small text-center text-white-50">
      <div class="container">
        Copyright &copy; Your Website 2018
      </div>
    </footer> -->




    <!-- viewdetails Modal -->
  <div class="modal fade client-mgmt-modal" id="viewdetails-modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Investment Amount</h4>
        </div>
        <div class="modal-body">
          <div class="d-flex">
            <div class="piechart-circle">
              <h6>PM Securities | Bull rider fund (AIF)</h6>
            </div>

            <div class="piechart-elements">
              <h6>2500000</h6>
            </div>
          </div>

          <div class="d-flex">
            <div class="piechart-circle">
              <h6>Mosl | Growth fund (PMS)</h6>
            </div>

            <div class="piechart-elements">
              <h6>10000000</h6>
            </div>
          </div>


          <div class="d-flex">
            <div class="piechart-circle">
              <h6>OLA (Private Equity) 1000 shares @ Rs.1234/share</h6>
            </div>

            <div class="piechart-elements">
              <h6>1000000</h6>
            </div>
          </div>

        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
</div>


<!-- add client Modal -->
  <div class="modal fade client-mgmt-modal" id="add-client-modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Client</h4>
        </div>
        <div class="modal-body form-inline">
          <form class="add-client-form">
            <div class="form-group d-flex">
              <label for="exampleFormControlInput1">Client Name</label>
              <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="">
            </div>
            <div class="form-group d-flex">
              <label for="exampleFormControlSelect1">Email ID</label>
              <input type="email" class="form-control" id="exampleFormControlInput2" placeholder="">
            </div>
            <div class="form-group d-flex">
              <label for="exampleFormControlSelect2">Phone No.</label>
              <input type="number" class="form-control" id="exampleFormControlInput31" placeholder="">
            </div>
            <!-- <div class="form-group d-flex">
              <input type="submit" class="form-control" id="submit" value="submit" placeholder="">
            </div> -->
            <div class="period-returns-risks save-client" id="">
              <button class="">Save</button>
            </div>
          </form>

        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
</div>


<!-- update Modal -->
  <div class="modal fade client-mgmt-modal" id="update-modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update </h4>
        </div>
        <div class="modal-body form-inline">
          <form class="add-client-form">
            <div class="form-group d-flex">
              <label for="exampleFormControlInput1">Client Name</label>
              <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="">
            </div>
            <div class="form-group d-flex">
              <label for="exampleFormControlSelect1">Email ID</label>
              <input type="email" class="form-control" id="exampleFormControlInput2" placeholder="">
            </div>
            <div class="form-group d-flex">
              <label for="exampleFormControlSelect2">Phone No.</label>
              <input type="number" class="form-control" id="exampleFormControlInput31" placeholder="">
            </div>
            <!-- <div class="form-group d-flex">
              <input type="submit" class="form-control" id="submit" value="submit" placeholder="">
            </div> -->
            <div class="period-returns-risks save-client" id="">
              <button class="">Save</button>
            </div>
          </form>

        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
</div>

<!-- viewprogress Modal -->
  <div class="modal fade client-mgmt-modal" id="viewprogress-modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Steps completed 5/8</h4>
        </div>
        <div class="modal-body">

          <div class="owl-carousel owl-theme">
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Investment request raised with fundsmap</h6>
                  <img class="status-icon" src="{{asset('images/checked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Documentation done with client</h6>
                  <img class="status-icon" src="{{asset('images/checked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Document verification in progress</h6>
                  <img class="status-icon" src="{{asset('images/checked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Investment request raised with fundsmap</h6>
                  <img class="status-icon" src="{{asset('images/checked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Investment request raised with fundsmap</h6>
                  <img class="status-icon" src="{{asset('images/checked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Investment request raised with fundsmap</h6>
                  <img class="status-icon" src="{{asset('images/unchecked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Investment request raised with fundsmap</h6>
                  <img class="status-icon" src="{{asset('images/unchecked.png')}}">
                </div>
              </div>
              <div class="item">
                <div class="steps-completed-block">
                  <img class="step-icon" src="{{asset('images/user.png')}}">
                  <h6>Investment request raised with fundsmap</h6>
                  <img class="status-icon" src="{{asset('images/unchecked.png')}}">
                </div>
              </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection