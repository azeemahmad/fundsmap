<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Welcome in Admin Panel
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2019 <a href="{{url('/')}}" target="_blank">Funds Map</a>.</strong> All rights reserved.
</footer>