@extends('admin.layouts.master')
@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">Agents</div>
        <div class="panel-body">
            <div class="col-md-12 page-action text-left">
                @can('add_agents')
                <a href="{{ url('/admin/agents/create') }}" class="btn btn-success btn-sm pull-right"
                   title="Add New Agent">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
                @endcan
            </div>
            <div class="col-md-12">
                <form method="GET" action="{{ url('/admin/agents') }}" accept-charset="UTF-8"
                      class="navbar-form navbar-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               value="{{ request('search') }}">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                </form>
            </div>
            <br/>
            <br/>

            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="data-table">
                        <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Mobile</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>City</th>
                            @can('view_agents','edit_agents', 'delete_agents')
                            <th>Actions</th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agents as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $item->first_name }}</td>
                                <td>{{ $item->last_name }}</td>
                                <td>{{ $item->mobile }}</td>
                                <td>{{ isset($item->country->name) ? $item->country->name : ''}}</td>
                                <td>{{ isset($item->state->name) ? $item->state->name : ''}}</td>
                                <td>{{ isset($item->city->name) ? $item->city->name : ''}}</td>
                                @can('view_agents')
                                <td>
                                    <a href="{{ url('/admin/agents/' . $item->id) }}" title="View Agents"><button class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                    @include('shared._actions', [
                                    'entity' => 'agents',
                                    'id' => $item->id
                                    ])
                                </td>
                                    @endcan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $agents->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>

            </div>
        </div>
    </div>
@endsection
