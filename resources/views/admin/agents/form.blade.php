<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Agents</b></div>
        <br/>

        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
            <label for="first_name" class="col-md-4 control-label">{{ 'First Name' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="first_name" type="text" id="title"
                       value="{{ isset($agent->first_name) ? $agent->first_name : ''}}">
                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
            <label for="last_name" class="col-md-4 control-label">{{ 'Last Name' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="last_name" type="text" id="title"
                       value="{{ isset($agent->last_name) ? $agent->last_name : ''}}">
                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
            <label for="mobile_code" class="col-md-4 control-label">{{ 'Mobile Number' }}</label>

            <div class="col-md-6">
                <div class="col-md-4">
                    <select name="mobile_code" class="form-control" id="mobile_code">
                        @foreach(App\Models\MobileCode::get() as $key => $value)
                            <option value="{{$value->international_code}}" @if($value->id==101) {{'selected'}} @endif>{{$value->country_code}}
                                (+{{$value->international_code}})
                            </option>
                        @endforeach
                    </select>

                </div>
                <div class="col-md-8">
                    <input type="number" name="mobile" class="form-control" value="{{$agent->mobile or ''}}" placeholder="Enter Phone Number...">
                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
            <label for="country_id" class="col-md-4 control-label">{{ 'Country' }}</label>

            <div class="col-md-6">
                <select name="country_id" class="form-control" id="country_id">
                    <option value="" id="hidecountry">Select Country</option>
                    @foreach(App\Models\Country::get() as $key => $value)
                        <option value="{{$value->id}}" @if(isset($agent->country_id) && $agent->country_id==$value->id) {{'selected'}} @endif>{{$value->name}}</option>
                    @endforeach
                </select>
                {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('state_id') ? 'has-error' : ''}}">
            <label for="state_id" class="col-md-4 control-label">{{ 'State' }}</label>
            <div class="col-md-6">
                <select name="state_id" class="form-control" id="state_id">
                    @if(isset($agent->state_id) && !empty($agent->state_id))
                        <option value="{{$agent->state_id}}">{{$agent->state->name}}</option>
                    @endif

                </select>
                {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('city_id') ? 'has-error' : ''}}">
            <label for="city_id" class="col-md-4 control-label">{{ 'City' }}</label>
            <div class="col-md-6">
                <select name="city_id" class="form-control" id="city_id">
                    @if(isset($agent->city_id) && !empty($agent->city_id))
                        <option value="{{$agent->city_id}}">{{$agent->city->name}}</option>
                    @endif
                </select>
                {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>

        $('#country_id').change(function(){
            $("#hidecountry").hide();
            var country_id=$('#country_id').val();

            $.ajax({
                url: "/agent/state",
                type: 'GET',
                data: {country_id: country_id},
                success: function (response) {
                    $('#state_id').html(response);
                }
            });

        });

        $('#state_id').change(function(){
            var state_id=$('#state_id').val();
            $.ajax({
                url: "/agent/city",
                type: 'GET',
                data: {state_id: state_id},
                success: function (response) {
                    $('#city_id').html(response);
                }
            });
        });

    </script>
@endsection
