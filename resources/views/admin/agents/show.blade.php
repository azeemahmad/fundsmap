@extends('admin.layouts.master')
<style>

    .imageheight{
        width: 100px;
        height: 100px;
    }
    img{
        -webkit-transition:all 0.5s;
        -moz-transition:all 0.5s;
        -ms-transition:all 0.5s;
        -o-transition:all 0.5s;
    }

    img:hover{
        width:400px;
        height:250px;
    }

</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Agent {{ $agent->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/agents') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        @can('edit_agents', 'delete_agents')
                        <a href="{{ url('/admin/agents/' . $agent->id . '/edit') }}" title="Edit Agent">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/agents' . '/' . $agent->id) }}" accept-charset="UTF-8"
                              style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Agent"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        @endcan
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $agent->id }}</td>
                                </tr>
                                <tr>
                                    <th> First Name</th>
                                    <td> {{ $agent->first_name }} </td>
                                </tr>
                                <tr>
                                    <th>Last Name</th>
                                    <td> {{ $agent->last_name }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    <td> {{ $agent->mobile }} </td>
                                </tr>
                                <tr>
                                    <th> Country</th>
                                    <td> {{ $agent->country->name }} </td>
                                </tr>
                                <tr>
                                    <th> State</th>
                                    <td> {{ $agent->state->name }} </td>
                                </tr>
                                <tr>
                                    <th> City</th>
                                    <td> {{ $agent->city->name }} </td>
                                </tr>

                                @if(isset($agntprofile) && !empty($agntprofile))
                                <tr>
                                    <th> Company Name</th>
                                    <td> {{ $agntprofile->compnay_name }} </td>
                                </tr>

                                <tr>
                                    <th> Associate Name</th>
                                    <td> {{ $agntprofile->associate_name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $agntprofile->email }} </td>
                                </tr>
                                <tr>
                                    <th> Bank Account</th>
                                    <td> {{str_repeat("*", strlen($agntprofile->account_no)-4) . substr($agntprofile->account_no, -4)}}</td>
                                </tr>
                                <tr>
                                    <th> IFSC Code</th>
                                    <td> {{ $agntprofile->ifsc_code }} </td>
                                </tr>
                                <tr>
                                    <th> Bank Name</th>
                                    <td> {{ $agntprofile->bank_name }} </td>
                                </tr>
                                <tr>
                                    <th> Branch Name</th>
                                    <td> {{ $agntprofile->branch_name }} </td>
                                </tr>
                                <tr>
                                    <th>Pan Card</th>
                                    <td> <img src="{{asset('PanCardImage/'.$agntprofile->pan)}}" class="imageheight"> </td>
                                </tr>
                                <tr>
                                    <th>Blank Cheque</th>
                                    <td> <img src="{{asset('ChequeImage/'.$agntprofile->cheque)}}" class="imageheight"> </td>
                                </tr>
                                    @endif
                                @if($agent->profile_image != null)
                                    <tr>
                                        <th>Agent Profile Picture</th>
                                        <td> <img src="{{asset('/images/AgentProfile/'.$agent->profile_image)}}" class="imageheight"> </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

