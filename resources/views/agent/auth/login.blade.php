@extends('agent.agentlayouts.master')
<style>
    .login-register-page-mobile{
        display: none;
    }
</style>

@section('usermaster')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <h4 class="logo-center"><a href="{{url('/')}}" style="color: white">FUNDS MAP</a></h4>
        </div>
    </nav>

    <section id="about" class="about-section text-center register-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h4 class="text-white mb-4 register-title">Register Now!</h4>
                    <div class="register-subtitle">
                        <h4 class="text-white">Please enter your</h4>
                        <h3 class="text-white">Phone Number</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($errors->first())
        <div class="alert alert-danger">
            {{ $errors->first() }}
        </div>
    @endif
    <section id="signup" class="signup-section register-details">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto text-center">

                    <h3 class="mb-5">Phone Number<span>*</span></h3>
                        <form class="form-inline" method="POST" action="{{ route('mobile.verification') }}">
                         <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="country-code">
                            <select name="countryCode" id="" class="form-control flex-fill">
                                <optgroup>
                                @foreach(App\Models\MobileCode::get() as $key => $value)
                                    <option  value="{{$value->international_code}}" @if($value->id==101) {{'selected'}} @endif>{{$value->country_code}} (+{{$value->international_code}})</option>
                                @endforeach
                                </optgroup>
                            </select>
                            @if ($errors->has('countryCode'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('countryCode') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <input type="number" name="mobile" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 enterphoneno" id="inputphoneno" placeholder="Enter Phone Number...">
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                            @endif
                        <button type="submit" class="btn btn-primary mx-auto next"><i class="fas fa-arrow-circle-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
