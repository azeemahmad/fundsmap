@extends('agent.agentlayouts.master')
<style>
    .login-register-page-mobile{
        display: none;
    }
    .error{
        color: red;
    }

    #selectcountry,#selectstate,#selectcity {
        font-size: 22px;

    }
</style>
@section('usermaster')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <h4 class="logo-center"><a href="{{url('/')}}" style="color: white">FUNDS MAP</a></h4>
        </div>
    </nav>
    .country-code.city-code select {
    width: 100%;
    }

<!-- About Section -->
<section id="about" class="about-section text-center register-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <!-- <h4 class="text-white mb-4 register-title">Register Now!</h4> -->
                <div class="register-subtitle">
                    <h4 class="text-white">Please enter your</h4><!-- text-white-50 -->
                    <h3 class="text-white">Details</h3>
                </div>
            </div>
        </div>
        <!-- <img src="img/ipad.png" class="img-fluid" alt=""> -->
    </div>
</section>

<!-- Signup Section -->
<section id="signup" class="signup-section register-details user-details">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">

                <form class="form-inline" method="POST" action="{{ route('user.register') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="d-flex user-details-row">
                        <div>
                            <h3 class="mb-5">First Name<span>*</span></h3>
                            <input type="text" name="first_name" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 enterfirstname"  placeholder="">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div>
                            <h3 class="mb-5">Last Name<span>*</span></h3>
                            <input type="text" name="last_name" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 enterlastname"  placeholder="">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                            <h3 class="mb-5">Your Country<span>*</span></h3>
                            <div class="country-code city-code">
                                <select name="country_id"  class="form-control flex-fill" id="selectcountry">
                                    <option value="" id="hidecountry">Select Country</option>
                                    @foreach(App\Models\Country::get() as $key => $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <h3 class="mb-5">Your State<span>*</span></h3>
                            <div class="country-code city-code">
                                <select name="state_id"  class="form-control flex-fill" id="selectstate">

                                </select>
                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                    <h3 class="mb-5">Your City<span>*</span></h3>
                    <div class="country-code city-code">

                        <select name="city_id"  class="form-control flex-fill" id="selectcity">

                        </select>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <h3 class="mb-5 password-text">Password<span>*</span></h3><div class="country-code city-code">

                    <input style="width: 100%;" type="password" name="password" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 "  placeholder="">
                    </div>
                       @if ($errors->has('password'))
                           <span class="help-block">
                         <strong>{{ $errors->first('password') }}</strong>
                         </span>
                       @endif


                    <div class="terms">
                        <div class="checkbox">
                            <label><input type="checkbox" name="term&condition" value="1" required>&nbsp; Yes, I accept all the terms & conditions. <a href="#">Read T&C</a>
                                @if ($errors->has('term&condition'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('term&condition') }}</strong>
                                    </span>
                                @endif
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mx-auto submit">SIGN IN</button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script>

        $('#selectcountry').change(function(){
            $("#hidecountry").hide();
            var country_id=$('#selectcountry').val();

            $.ajax({
                url: "/agent/state",
                type: 'GET',
                data: {country_id: country_id},
                success: function (response) {
                        $('#selectstate').html(response);
                }
            });

        });

        $('#selectstate').change(function(){
            var state_id=$('#selectstate').val();
            $.ajax({
                url: "/agent/city",
                type: 'GET',
                data: {state_id: state_id},
                success: function (response) {
                    $('#selectcity').html(response);
                }
            });
        });

    </script>
@endsection
