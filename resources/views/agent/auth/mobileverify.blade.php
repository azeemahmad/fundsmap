@extends('agent.agentlayouts.master')
<style>
    .login-register-page-mobile{
        display: none;
    }
</style>
@section('usermaster')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <h4 class="logo-center"><a href="{{url('/')}}" style="color: white">FUNDS MAP</a></h4>
        </div>
    </nav>

<section id="about" class="about-section text-center register-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <!-- <h4 class="text-white mb-4 register-title">Register Now!</h4> -->
                <div class="register-subtitle">
                    <h4 class="text-white">OTP has been sent on</h4><!-- text-white-50 -->
                    <h3 class="text-white">+{{$data['countryCode']}} {{$data['mobile']}}</h3>
                </div>
            </div>
        </div>
        <!-- <img src="img/ipad.png" class="img-fluid" alt=""> -->
    </div>
</section>
<section id="signup" class="signup-section register-details">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">

                <h3 class="mb-5">Please enter your OTP</h3>

                <form class="form-inline otp-form" action="javascript:void(0)">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div id="divOuter">
                        <div id="divInner">
                            <input id="partitioned" value="" type="number" name="otp" maxlength="5" autocomplete="off"/>
                        </div>
                    </div>
                    <div id="dispalymessage"></div>

                    <button type="submit" class="btn btn-primary mx-auto next" id="submitotpbutton"><i class="fas fa-arrow-circle-right"></i></button>
                </form>


            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    <script>
$('#submitotpbutton').click(function(){
    $('#dispalymessage').html('');
    var otp= $('#partitioned').val();
    var mobile= '{{$data['mobile']}}';
    var mobilecode='{{$data['countryCode']}}';
    if(otp=='' || otp ==null){
        $('#dispalymessage').html('<br>Enter otp first !');
        $('#dispalymessage').css('color','red');
    }
    else if(otp.length <5){
        $('#dispalymessage').html('<br>Enter 5 digit otp number !');
        $('#dispalymessage').css('color','red');
    }
    else{
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/agent/otp_verification",
            type: 'POST',
            data: {otp: otp,mobile:mobile,mobilecode:mobilecode},
            success: function (response) {
                if (response==0) {
                    $('#dispalymessage').html('<br>Invalid user !');
                    $('#dispalymessage').css('color','red');
                }
                else if(response==1){
                    $('#dispalymessage').html('<br>OTP sesseion expire.<br>Please retry!');
                    $('#dispalymessage').css('color','red');
                }
                else if(response==2){
                    $('#dispalymessage').html('<br>Please enter valid OTP');
                    $('#dispalymessage').css('color','red');
                }
                else if(response==3){
                    $(location).attr('href', '{{env('APP_URL').'/agent/user_register'}}')
                }
                else{
                    $('#dispalymessage').html('<br>Something wrong!');
                    $('#dispalymessage').css('color','red');
                }
            }
        });
    }


});
    </script>
@endsection