@extends('agent.agentlayouts.master')
<style>
    img{
        -webkit-transition:all 0.5s;
        -moz-transition:all 0.5s;
        -ms-transition:all 0.5s;
        -o-transition:all 0.5s;
    }

    img:hover{
        width:400px;
        height:250px;
    }
    /*@media(max-width: 768px){*/
    .belowagent{
        margin-top: 70px
    }
    .backbutton{
        width: 100px !important;
        height: 33px;
        padding: 0 !important;
        /*margin-left: 540px;*/
    }
    .backbuttonedit{
        width: 100px !important;
        height: 33px;
        padding: 0 !important;
        margin-left: 420px;
        background-color:goldenrod;
    }
    .agentfont{
        font-size: 32px;
        color: royalblue;
    }
    .imageheight{
        width: 100px;
        height: 100px;
    }


    /*}*/
    @media(max-width: 540px){
        .backbuttonedit {
            margin-left: 243px;
        }
        .backbutton {
            width: 69px !important;
            height: 30px;
            margin-bottom: -34px;
        }
    }
    @media(max-width: 480px){
        .backbuttonedit {
            width: 67px !important;
            height: 25px;
            padding: 0 !important;
            margin-left: 224px !important;
            background-color: goldenrod;
        }
        .backbutton {
            height: 26px;
        }
    }

</style>
@section('usermaster')
    <div class="container">
        <div class="row">
            <div class="col-md-12 belowagent">
                <div class="panel panel-default">
                    <div class="panel-heading agentfont">{{ ucwords($agent->first_name).' '.ucwords($agent->last_name) }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/agent/dashboard') }}" title="Back">
                            <button class="btn btn-success btn-xs backbutton" ><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>

                        <a href="{{ url('/agent/userprofile') }}" title="Edit">
                            <button class="btn backbuttonedit" ><i class="fa fa-edit" aria-hidden="true"></i>
                                Edit
                            </button>
                        </a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>

                                <tr>
                                    <th> First Name</th>
                                    <td> {{ $agent->first_name }} </td>
                                </tr>
                                <tr>
                                    <th>Last Name</th>
                                    <td> {{ $agent->last_name }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    <td> {{ $agent->mobile }} </td>
                                </tr>
                                <tr>
                                    <th> Country</th>
                                    <td> {{ $agent->country->name }} </td>
                                </tr>
                                <tr>
                                    <th> State</th>
                                    <td> {{ $agent->state->name }} </td>
                                </tr>
                                <tr>
                                    <th> City</th>
                                    <td> {{ $agent->city->name }} </td>
                                </tr>
                                <tr>
                                    <th> Company Name</th>
                                    <td> {{ $agntprofile->compnay_name }} </td>
                                </tr>
                                <tr>
                                    <th> Associate Name</th>
                                    <td> {{ $agntprofile->associate_name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $agntprofile->email }} </td>
                                </tr>
                                <tr>
                                    <th> Bank Account</th>
                                    <td>  {{str_repeat("*", strlen($agntprofile->account_no)-4) . substr($agntprofile->account_no, -4)}} </td>
                                </tr>
                                <tr>
                                    <th> IFSC Code</th>
                                    <td> {{ $agntprofile->ifsc_code }} </td>
                                </tr>
                                <tr>
                                    <th> Bank Name</th>
                                    <td> {{ $agntprofile->bank_name }} </td>
                                </tr>
                                <tr>
                                    <th> Branch Name</th>
                                    <td> {{ $agntprofile->branch_name }} </td>
                                </tr>
                                <tr>
                                    <th>Pan Card</th>
                                    <td> <img src="{{asset('PanCardImage/'.$agntprofile->pan)}}" class="imageheight"> </td>
                                </tr>
                                <tr>
                                    <th>Blank Cheque</th>
                                    <td> <img src="{{asset('ChequeImage/'.$agntprofile->cheque)}}" class="imageheight"> </td>
                                </tr>
                                @if($agent->profile_image != null)
                                    <tr>
                                        <th>Agent Profile Picture</th>
                                        <td> <img src="{{asset('/images/AgentProfile/'.$agent->profile_image)}}" class="imageheight"> </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection