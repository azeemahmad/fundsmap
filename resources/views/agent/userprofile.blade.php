@extends('agent.agentlayouts.master')
<style>
    .login-register-page-mobile {
        display: none;
    }
    @media (max-width: 768px) {
        .profile_image_logo {
            width: 139px;
            height: 129px !important;
            border-radius: 100px;
            margin-bottom: -158px !important;
        }
    }

    @media(max-width: 540px){
        .profile_image_logo {
            width: 111px;
            height: 107px !important;
            margin-bottom: -98px !important;
        }
    }
    @media(max-width: 430px){
        .profile_image_logo {
            width: 98px;
            height: 103px !important;
            margin-bottom: -85px !important;
        }
    }
</style>
@section('usermaster')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <h4 class="logo-center"><a href="{{url('/')}}" style="color: white">FUNDS MAP</a></h4>
        </div>
    </nav>

    <!-- About Section -->
    <section id="about" class="about-section text-center register-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h4 class="text-white mb-4 register-title">Associate Profile</h4>

                </div>
            </div>
            <form id="datafiles" action="javascript:void(0)" enctype="multipart/form-data">
                {{csrf_field()}}
            <div class="company-logo-outer">

                @if(isset($agent) && $agent->profile_image != null)
                    <img src="{{asset('/images/AgentProfile/'.$agent->profile_image)}}" class="img-fluid company-logo profile_image_logo" alt="">
                @else
                    <img src="{{asset('images/user.png')}}" class="img-fluid company-logo" alt="">
                @endif
                <input type="file" name="profile_image" id="company-logo"><label for="company-logo"><i
                            class="fas fa-camera edit-company-logo"></i></label>
            </div>
            </form>
        </div>
    </section>
    <!-- Signup Section -->
    <section id="signup" class="signup-section register-details user-details associate-profile">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto text-center">

                    <form class="form-inline" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <h3 class="mb-5 password-text">Company Name<span>*</span></h3>
                        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 " id=""
                               placeholder="" name="compnay_name" value="{{ isset($agntprofile->compnay_name) ? $agntprofile->compnay_name : ''}}" required>
                     <div>
                         @if ($errors->has('compnay_name'))
                             <span class="help-block">
                                        <strong>{{ $errors->first('compnay_name') }}</strong>
                                    </span>
                         @endif
                     </div>
                        <h3 class="mb-5 password-text">Name Of Associate<span>*</span></h3>
                        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 " id=""
                               placeholder="" name="associate_name" value="{{ isset($agntprofile->associate_name) ? $agntprofile->associate_name : ''}}" required>
                        @if ($errors->has('associate_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('associate_name') }}</strong>
                                    </span>
                        @endif

                        <h3 class="mb-5 password-text">Email ID<span>*</span></h3>
                        <input type="email" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 " id=""
                               placeholder="" name="email" value="{{ isset($agntprofile->email) ? $agntprofile->email : ''}}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif

                       @if(!$agntprofile)
                        <h3 class="mb-5 password-text">Verification Code<span>*</span></h3>

                        <div id="divOuter">
                            <div id="divInner">
                                <input id="partitioned" name="verificationcode" type="text" maxlength="5"/>
                            </div>
                        </div>
                        <p class="mb-5">Resend Code. Code will be valid for 5 Minutes.<span>*</span></p>
                        @endif

                        <div class="container bank-details">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseOne">
                                                Bank Details
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">

                                            {{--<form class="form-inline form-inline1">--}}

                                                <h3 class="mb-5 password-text">Account No.<span>*</span></h3>
                                                <input type="number"
                                                       class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 " id=""
                                                       placeholder="" name="account_no" value="{{ isset($agntprofile->account_no) ? $agntprofile->account_no : ''}}" required>
                                            @if ($errors->has('account_no'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('account_no') }}</strong>
                                    </span>
                                            @endif

                                                <h3 class="mb-5 password-text">IFSC Code<span>*</span></h3>
                                                <input type="text" id="ifsc_code_details"
                                                       class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 "
                                                       placeholder="" name="ifsc_code" value=" {{ isset($agntprofile->ifsc_code) ? $agntprofile->ifsc_code : ''}}" maxlength="12" required>
                                            @if ($errors->has('ifsc_code'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('ifsc_code') }}</strong>
                                    </span>
                                            @endif

                                                <h3 class="mb-5 password-text">Bank Name<span>*</span></h3>
                                                <input type="text"
                                                       class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 disabled"
                                                       id="bank_name_details" placeholder="" name="bank_name" value="{{ isset($agntprofile->bank_name) ? $agntprofile->bank_name : ''}}" required>
                                            @if ($errors->has('bank_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('bank_name') }}</strong>
                                    </span>
                                            @endif

                                                <h3 class="mb-5 password-text">Branch Name<span>*</span></h3>
                                                <input type="text"
                                                       class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0 disabled"
                                                       id="branch_name_details" placeholder="" name="branch_name" value="{{ isset($agntprofile->branch_name) ? $agntprofile->branch_name : ''}}" required>
                                            @if ($errors->has('branch_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('branch_name') }}</strong>
                                    </span>
                                            @endif
                                            {{--</form>--}}

                                            <p>
                                                <button type="button" class="incorrect-details btn btn-link">Click here if above
                                                    details are incorrect
                                                </button>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end container -->

                        <div class="d-flex pan-cheque">
                            <div class="file btn btn-lg btn-primary upload-pan-cheque">
                                Upload PAN Card
                                <input type="file" name="pan" @if(!$agntprofile) required @endif />
                                @if ($errors->has('pan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="file btn btn-lg btn-primary upload-pan-cheque">
                                Upload Cancelled Cheque
                                <input type="file" name="cheque" @if(!$agntprofile) required @endif />
                                @if ($errors->has('cheque'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cheque') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mx-auto submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".incorrect-details").click(function () {
                $("input").removeClass("disabled");
            });

            $("#ifsc_code_details").keyup(function(){
                var length=$(this).val().length;
                $('#bank_name_details').val('');
                $('#branch_name_details').val('');
                var code=$(this).val();
                if(length > 10){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/agent/ifsccode",
                        type: 'POST',
                        data: {code: code},
                        success: function (response) {
                        console.log(response);
                            if(response != 0){
                                $('#bank_name_details').val(response[0]['bankname']);
                                $('#branch_name_details').val(response[0]['branch']);
                            }
                            else{
                                $('#bank_name_details').val('');
                                $('#branch_name_details').val('');
                            }
                        }
                    });
                }
            });


        });

        $('#company-logo').change(function(){

            var image = $("#company-logo").val();
            var extension = image.split('.').pop().toUpperCase();
            if (image == '') {
                alert('Upload Image');
                return false;
            }
            else if (extension != "PNG" && extension != "JPG" && extension != "GIF" && extension != "JPEG") {
                alert('Please upload file having extensions .jpeg/.jpg/.gif/.png');
                return false;
            }

            $('form#datafiles').submit();
            var form = $('form#datafiles')[0];
            var formData = new FormData(form);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/agent/saveprofileimage",
                type: 'POST',
                data: formData,
                success: function (data) {
                    if(data==1){
                        location.reload();
                    }
                    else{
                        location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
    </script>
@endsection