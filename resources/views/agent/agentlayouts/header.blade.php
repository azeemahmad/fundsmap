<style>
    .loginregister{
        font-size: 16px;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light fixed-top login-register-page-mobile" id="mainNav">
    <div class="container">
        <!-- <a class="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a> -->
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <h4 class="logo"><a href="{{url('/')}}" style="color: white">FUNDS MAP</a></h4>

        <div class="header-right-block d-flex">
            <i class="fas fa-eye"></i>
            <div class="dropdown user-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-circle"></i> </a><!-- <span class="caret"></span> -->

                <ul class="dropdown-menu">
                    @if(Auth::guard('agent')->check())
                        <?php
                            $agentid=Auth::guard('agent')->user()->id;
                            $agentprofile=App\Models\AgentProfile::where('agent_id',$agentid)->first();
                            ?>
                       @if($agentprofile)
                        <li><a href="{{url('/agent/completeuserprofile')}}">User Profile</a></li>
                        @else
                        <li><a href="{{url('/agent/userprofile')}}">User Profile</a></li>
                        @endif
                    <li><a href="{{url('/agent/userlogout')}}">Logout</a></li>
                    @else
                        <li><a href="{{url('/agent/login')}}" class="loginregister">Login</a></li>
                    @endif
                </ul>
            </div>
        </div>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link js-scroll-trigger" href="#about">Partner With Us</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link js-scroll-trigger" href="#projects">Funds Map Advantage</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link js-scroll-trigger" href="#">Contact</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{url('/relationship-manager/client-manager')}}">Client Manager</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{url('/relationship-manager/relationship-manager')}}">Relationship manager</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{url('/agent/dashboard')}}">Agent Dashboard</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
