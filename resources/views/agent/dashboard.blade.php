@extends('agent.agentlayouts.master')
@section('usermaster')
    <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">

            </div>
        </div>
    </header>
    <section id="about" class="search-section text-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 mx-auto">
                    <div class="social d-flex justify-content-center">
                        <div class="custom-search-input" id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control input-lg search-field" placeholder="SEARCH"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="fas fa-search"></i>
                            <!-- <i class="glyphicon glyphicon-search"></i> -->
                        </button>
                    </span>
                            </div>
                        </div>

                        <div class="filter" id="">
                            <i class="fas fa-filter"></i>
                        </div>

                        <div class="small-cases-btn" id="">
                            <div class="input-group col-md-12">
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            Small Cases
                        </button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <img src="img/ipad.png" class="img-fluid" alt=""> -->
        </div>
    </section>

    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light dashboard-tabs">
        <div class="container-fluid">

            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_default_1" data-toggle="tab" class="active show">
                                SEARCH FUNDS </a>
                        </li>
                        <li>
                            <a href="#tab_default_2" data-toggle="tab">
                                MANAGE CLIENTS </a>
                        </li>
                        <li>
                            <a href="#tab_default_3" data-toggle="tab">
                                MY PROFILE </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_default_1">

                            <!-----------------search-funds started here------------------>
                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-logo" id="">
                                        <img src="https://www.motilaloswalmf.com/Campaigns/Dynamic/images/logo.gif">
                                    </div>

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>bull rider fund</h4>
                                            </div>
                                            <div class="company-details-2 star">
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h4>rank <span>#12</span></h4>
                                            </div>
                                        </div>

                                        <div class="social d-flex justify-content-center funds-row">
                                            <div class="company-details-1 fund-title d-flex">
                                                <div class="sub-title-1"><h5 class="fund-text">avg. returns</h5></div>
                                                <div class="sub-title-2"><h5 class="fund-percentage">22.34%</h5></div>
                                            </div>
                                            <div class="company-details-2 divider">
                                                <div class="inner-divider"></div>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <p class="funds-details">since inception returns.<br>inception
                                                    date:21|01|11</p>
                                            </div>
                                        </div>

                                        <div class="social d-flex justify-content-center funds-row">
                                            <div class="company-details-1 fund-title d-flex">
                                                <div class="sub-title-1"><h5 class="fund-text">risk type</h5></div>
                                                <div class="sub-title-2"><h5 class="fund-risk-text">above average</h5>
                                                </div>
                                            </div>
                                            <div class="company-details-2 divider">
                                                <div class="inner-divider"></div>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <p class="funds-details">since inception risk.<br>volatility of
                                                    returns:16.23%</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">period</h4>
                                        <ul class="year">
                                            <li>1 year</li>
                                            <li>3 year</li>
                                            <li>5 year</li>
                                        </ul>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">returns</h4>
                                        <ul class="returns">
                                            <li>13%</li>
                                            <li>25.23%</li>
                                            <li>21.01%</li>
                                        </ul>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">risks</h4>
                                        <ul class="risks">
                                            <li class="above-avg">above avg.</li>
                                            <li class="average">average</li>
                                            <li class="below-avg">below avg.</li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="fund-managers-profile" id="">
                                        <h4 class="">fund manager's profile</h4>
                                        <ul class="year d-flex">
                                            {{--{{asset('')}}--}}
                                            <img src="{{asset('images/fmp1.png')}}">
                                            <img src="{{asset('images/fmp2.png')}}">
                                            <img src="{{asset('images/fmp3.png')}}">
                                            <img src="{{asset('images/fmp1.png')}}">
                                        </ul>
                                    </div>
                                </div>


                                <div class="social d-flex justify-content-center border-radius-bottom share-row">

                                    <div class="" id="">
                                        <ul class="d-flex">
                                            <a href="#" class="" data-toggle="modal" data-target="#piechart-modal"><i
                                                        class="fas fa-chart-pie"></i></a>
                                            <i class="fas fa-file-download"></i>
                                            <a href="#" class="" data-toggle="modal" data-target="#share-modal"><i
                                                        class="fas fa-share-alt"></i></a>
                                            <i class="fas fa-plus-circle"></i>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--search-funds-tab-outer-ends here-->


                            <div class="search-funds-tab-outer">
                                <div class="social d-flex justify-content-center search-funds-tab border-radius-top">

                                    <div class="company-logo" id="">
                                        <img src="https://www.motilaloswalmf.com/Campaigns/Dynamic/images/logo.gif">
                                    </div>

                                    <div class="company-details" id="">
                                        <div class="social d-flex justify-content-center">
                                            <div class="company-details-1 fund-title">
                                                <h4>bull rider fund</h4>
                                            </div>
                                            <div class="company-details-2 star">
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <h4>rank <span>#12</span></h4>
                                            </div>
                                        </div>

                                        <div class="social d-flex justify-content-center funds-row">
                                            <div class="company-details-1 fund-title d-flex">
                                                <div class="sub-title-1"><h5 class="fund-text">avg. returns</h5></div>
                                                <div class="sub-title-2"><h5 class="fund-percentage">22.34%</h5></div>
                                            </div>
                                            <div class="company-details-2 divider">
                                                <div class="inner-divider"></div>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <p class="funds-details">since inception returns.<br>inception
                                                    date:21|01|11</p>
                                            </div>
                                        </div>

                                        <div class="social d-flex justify-content-center funds-row">
                                            <div class="company-details-1 fund-title d-flex">
                                                <div class="sub-title-1"><h5 class="fund-text">risk type</h5></div>
                                                <div class="sub-title-2"><h5 class="fund-risk-text">above average</h5>
                                                </div>
                                            </div>
                                            <div class="company-details-2 divider">
                                                <div class="inner-divider"></div>
                                            </div>
                                            <div class="company-details-3 rank">
                                                <p class="funds-details">since inception risk.<br>volatility of
                                                    returns:16.23%</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">period</h4>
                                        <ul class="year">
                                            <li>1 year</li>
                                            <li>3 year</li>
                                            <li>5 year</li>
                                        </ul>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">returns</h4>
                                        <ul class="returns">
                                            <li>13%</li>
                                            <li>25.23%</li>
                                            <li>21.01%</li>
                                        </ul>
                                    </div>

                                    <div class="period-returns-risks" id="">
                                        <h4 class="">risks</h4>
                                        <ul class="risks">
                                            <li class="above-avg">above avg.</li>
                                            <li class="average">average</li>
                                            <li class="below-avg">below avg.</li>
                                        </ul>
                                    </div>

                                </div>


                                <div class="social d-flex justify-content-center search-funds-tab-1">

                                    <div class="fund-managers-profile" id="">
                                        <h4 class="">fund manager's profile</h4>
                                        <ul class="year d-flex">
                                            <img src="{{asset('images/fmp1.png')}}">
                                            <img src="{{asset('images/fmp2.png')}}">
                                            <img src="{{asset('images/fmp3.png')}}">
                                            <img src="{{asset('images/fmp1.png')}}">
                                        </ul>
                                    </div>

                                </div>


                                <div class="social d-flex justify-content-center border-radius-bottom share-row">

                                    <div class="" id="">
                                        <ul class="d-flex">
                                            <a href="#" class="showpiechart"><i
                                                        class="fas fa-chart-pie"></i></a>
                                            <i class="fas fa-file-download"></i>
                                            <a href="#" class="" data-toggle="modal" data-target="#share-modal"><i
                                                        class="fas fa-share-alt"></i></a>
                                            <i class="fas fa-plus-circle"></i>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--search-funds-tab-outer-ends here-->

                            <!-----------------search-funds ends here------------------>


                        </div>
                        <div class="tab-pane" id="tab_default_2">
                            <p>
                                Howdy, I'm in Tab 2.
                            </p>

                            <p>
                                Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                                nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud
                                exerci tation.
                            </p>

                            <p>
                                <a class="btn btn-warning" href="http://j.mp/metronictheme" target="_blank">
                                    Click for more features...
                                </a>
                            </p>
                        </div>
                        <div class="tab-pane" id="tab_default_3">
                            <p>
                                Howdy, I'm in Tab 3.
                            </p>

                            <p>
                                Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam,
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                                consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                                consequat
                            </p>

                            <p>
                                <a class="btn btn-info" href="http://j.mp/metronictheme" target="_blank">
                                    Learn more...
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="piechart-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pie Chart</h4>
                </div>
                <div class="modal-body">
                    {{--<div class="d-flex">--}}

                        {{--<div class="piechart-circle">--}}
                            {{--<img src="{{asset('images/piechart.png')}}">--}}
                            {!! $chart->html() !!}
                        {{--</div>--}}
                        {{--<div class="piechart-elements">--}}
                            {{--<ul class="elements">--}}
                                {{--<li><span class="img-block1"></span>above avg.</li>--}}
                                {{--<li><span class="img-block2"></span>average</li>--}}
                                {{--<li><span class="img-block3"></span>below avg.</li>--}}
                                {{--<li><span class="img-block4"></span>above avg.</li>--}}
                                {{--<li><span class="img-block5"></span>average</li>--}}
                                {{--<li><span class="img-block6"></span>below avg.</li>--}}
                                {{--<li><span class="img-block7"></span>above avg.</li>--}}
                                {{--<li><span class="img-block8"></span>average</li>--}}
                                {{--<li><span class="img-block9"></span>below avg.</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
        {!! Charts::scripts() !!}
        {!! $chart->script() !!}
    </div>





    <!-- share Modal -->
    <div class="modal fade" id="share-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Share</h4>
                </div>
                <div class="modal-body">
                    <p>share to all</p>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.showpiechart').click(function(){
            {{--$.ajax({--}}
                {{--url: '/agent/showpiechart',--}}
                {{--type: 'GET',--}}
                {{--success: function (data) {--}}
                    {{--if(data==1) {--}}
                        {{--$('.showpiechartdata').html({!! $chart->html() !!});--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}
            $(".showpiechart").attr("data-toggle", "modal");
            $(".showpiechart").attr("data-target", "#piechart-modal");

        });
    </script>
@endsection