@extends('agent.agentlayouts.master')
@section('usermaster')
    <header class="masthead">
        <div class="container h-100 align-items-center fundsmap-video-outer">
            <div class="mx-auto text-center">
                <div class="fundsmap-video">
                    <iframe width="100%" height="" src="https://www.youtube.com/embed/QvCGc6dnBtw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </header>
<section id="about" class="about-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h4 class="text-white mb-4 about-title">Partner with us <br>As a financial Advisor.</h4>
                <p class="text-white-50">Cater to tje needs of high net-worth individuals (HNI'S) & corporates. offer your clients the best of these services with minimal effort:</p>
                <p class="text-white-50">Portfolio management services (PMS)<br>Alternate investment funds (AIF)<br>Private equity</p>
            </div>
        </div>
        <!-- <img src="img/ipad.png" class="img-fluid" alt=""> -->
    </div>
</section>
<!-- Projects Section -->
<section id="projects" class="projects-section bg-light">
    <div class="container">

        <!-- Featured Project Row -->
        <div class="row align-items-center no-gutters mb-4 mb-lg-5 flickity-home-outer">
            <div class="col-xl-4 col-lg-5 advantage">
                <div class="featured-text text-center text-lg-left">
                    <h5>Funds Map Advantage</h5>
                    <p class="text-black-50 mb-0">
                        <!-- Flickity HTML init -->
                    <div class="carousel advantage-slider" data-flickity='{ "autoPlay": true }'>
                        <div class="carousel-cell"><img src="images/advantage-1.png"></div>
                        <div class="carousel-cell"><img src="images/advantage-1.png"></div>
                        <div class="carousel-cell"><img src="images/advantage-1.png"></div>
                        <div class="carousel-cell"><img src="images/advantage-1.png"></div>
                        <div class="carousel-cell"><img src="images/advantage-1.png"></div>
                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="projects-section bg-light your-role partner-role">
    <!-- Project One Row -->
    <div class="row justify-content-center no-gutters mb-5 mb-lg-0 your-role">
        <div class="col-lg-6 left-block">
            <div class="bg-black text-center h-100 project">
                <div class="d-flex h-100">
                    <div class="project-text w-100 my-auto text-center text-lg-left">
                        <h4 class="text-white text-left">Your Role<br>As a partner</h4>
                        <h4 class="mb-0 text-white-50 sub-title text-left">use fundmap's platform to identify the right pms/aif/pe.</h4>
                        <p class="text-left">Our platform has exhaustive detailing, comparisons & analysis.</p>
                        <h4 class="mb-0 text-white-50 sub-title text-left">share summarized documents with your clients.</h4>
                        <p class="text-left">Created by FUNDS MAP to explain your clients about thePMS/AIF/PE that you find suitable for them.</p>
                        <h4 class="mb-0 text-white-50 sub-title text-left">click invest button on your fundsmap portal.</h4>
                        <p class="text-left">& let funds maptake care of the rest.</p>
                        <h4 class="mb-0 text-white-50 sub-title text-left">receive your remuneration.</h4>
                        <hr class="d-none d-lg-block mb-0 ml-0">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 right-block">
            <img class="img-fluid" src="images/your-role-1.png" alt="">
        </div>

    </div>
</section>
<section class="projects-section bg-light your-role funds-map-role">
    <!-- Project Two Row -->
    <div class="row justify-content-center no-gutters ">
        <div class="col-lg-6 right-block">
            <img class="img-fluid" src="images/your-role-2.png" alt="">
        </div>
        <div class="col-lg-6 order-lg-first left-block">
            <div class="bg-black text-center h-100 project">
                <div class="d-flex h-100">
                    <div class="project-text w-100 my-auto text-center text-lg-right">
                        <h4 class="text-white text-right">Funds map's role</h4>
                        <h4 class="mb-0 text-white-50 sub-title text-right">detailed analysis & data of all pms/aif/pe's.</h4>
                        <h4 class="mb-0 text-white-50 sub-title text-right">manage processing & paper-work on your behalf.</h4>
                        <p class="text-right">While you focus on growing your business.</p>
                        <h4 class="mb-0 text-white-50 sub-title text-right">platform for you to manage your client relationships & business.</h4>
                        <p class="text-right">& let funds map take care of the rest.</p>
                        <h4 class="mb-0 text-white-50 sub-title text-right">support from experts on all your clients queries & concerns.</h4>
                        <hr class="d-none d-lg-block mb-0 mr-0">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="projects-section bg-light help-desk">
    <div class="whatsapp">
        <p class="whatsapp-button"><i class="fab fa-whatsapp"></i><span class="whatsapp-text">WHATS APP <br>HELP DESK</span></p>
    </div>
</section>
@endsection