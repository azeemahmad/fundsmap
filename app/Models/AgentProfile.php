<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentProfile extends Model
{

    protected $primaryKey = 'id';

    protected $fillable = [
        'agent_id','compnay_name', 'email', 'associate_name','verificationcode','account_no','ifsc_code','bank_name','branch_name','pan','cheque'
    ];

    public function agent(){
        return $this->belongsTo('App\Agent');
    }

}