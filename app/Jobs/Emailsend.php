<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class Emailsend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data=$this->data;
        try {
            Mail::send($data['view'], ['data' => $data], function ($message) use ($data) {
                $message->from($data['from_email'], $data['title']);
                $message->to($data['to_email'])->subject($data['subject']);
            });
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }
}
