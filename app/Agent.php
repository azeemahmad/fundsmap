<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Agent extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','email_verified_at','mobile_verified_at','remember_token','mobile_code','mobile','otp','city_id','state_id','country_id','profile_image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function country(){
        return $this->belongsTo('App\Models\Country');
    }
    public function state(){
        return $this->belongsTo('App\Models\State');
    }
    public function city(){
        return $this->belongsTo('App\Models\City');
    }
}
