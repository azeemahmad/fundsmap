<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfRelationManagerAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::guard('re_manager')->check()) {
            return redirect('/relationship-manager/home');
        }
        return $next($request);
    }
}
