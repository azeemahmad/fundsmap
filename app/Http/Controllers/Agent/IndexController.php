<?php

namespace App\Http\Controllers\Agent;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Illuminate\Http\Request;
use ConsoleTVs\Charts\Facades\Charts;


class IndexController extends Controller
{
    public function index(){
        return view('agent.index');
    }

    public function state(Request $request){
        $country_id=$request['country_id'];
        $state=State::where('country_id',$country_id)->get();
        $html='';
        if(isset($state) && $state->isNotEmpty()){
            foreach($state as $key => $value){
                $html .='<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }
        else{
            $html .='<span style="color:red">No state found !</span>';
        }
        return $html;

    }

    public function city(Request $request){
        $state_id=$request['state_id'];
        $city=City::where('state_id',$state_id)->get();
        $html='';
        if(isset($city) && $city->isNotEmpty()){
            foreach($city as $key => $value){
                $html .='<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }
        else{
            $html .='<span style="color:red">No city found !</span>';
        }
        return $html;

    }

    public function chart(){

        $blockbuster=10;
        $superhit=35;
        $hit=67;
        $average=20;
        $flop=78;

        $per=73;
        $rem=27;
        $name='Azeem Ahmad';

            $chart=Charts::create('pie', 'highcharts')
            ->title('K-Mean Clustering Algorithm for All Movie')
            ->labels(['Blockbuster','SuperHit', 'Hit', 'Average','Flop'])
            ->colors(['#00ff80', '#00ffff','#bf00ff', '#ffff00','#ff8000'])
            ->values([$blockbuster,$superhit,$hit,$average,$flop])
            ->dimensions(1000,500)
            ->responsive(false);


             $prediction=Charts::create('pie', 'highcharts')
            ->title('Decision Making Algorithm for Future Movie')
            ->labels(['Successful','Unsuccessful'])
            ->colors(['#125c0e','#ff0000'])
            ->values([$per,$rem])
            ->dimensions(1000,500)
            ->responsive(false);

        return view('chart',compact('chart','prediction','name'));
    }


}