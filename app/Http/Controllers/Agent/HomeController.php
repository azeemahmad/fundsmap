<?php

namespace App\Http\Controllers\Agent;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\AgentProfile;
use Illuminate\Support\Facades\Auth;
use ConsoleTVs\Charts\Facades\Charts;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('agent_auth');
    }

    public function dashboard(){

        $blockbuster=10;
        $superhit=35;
        $hit=67;
        $average=20;
        $flop=78;
        $chart=Charts::create('pie', 'highcharts')
            ->title('K-Mean Clustering Algorithm for All Movie')
            ->labels(['Blockbuster','SuperHit', 'Hit', 'Average','Flop'])
            ->colors(['#00ff80', '#00ffff','#bf00ff', '#ffff00','#ff8000'])
            ->values([$blockbuster,$superhit,$hit,$average,$flop]);
            /*->dimensions(650,380)*/
           // ->responsive(false);

        return view('agent.dashboard',compact('chart'));
    }
    public function userprofile(){
        $agent=Auth::guard('agent')->user();
        $agntprofile=AgentProfile::where('agent_id',$agent->id)->first();
        return view ('agent.userprofile',compact('agent','agntprofile'));
    }
    public function saveuserprofile(Request $request){


        $agent=Auth::guard('agent')->user();
        $agntprofile=AgentProfile::where('agent_id',$agent->id)->first();
        if(!$agntprofile){
            $this->validate($request, [
                'compnay_name' => 'required',
                'email' => 'required|email|unique:agent_profiles',
                'associate_name' => 'required',
                'account_no' => 'required',
                'ifsc_code' => 'required',
                'bank_name' => 'required',
                'branch_name' => 'required',
                'pan' => 'required|image',
                'cheque' => 'required|image',
            ]);
        }
        else{
            $this->validate($request, [
                'compnay_name' => 'required',
                'email' => 'required|email|unique:agent_profiles,email,'.$agntprofile->id,
                'associate_name' => 'required',
                'account_no' => 'required',
                'ifsc_code' => 'required',
                'bank_name' => 'required',
                'branch_name' => 'required',
            ]);
        }

        $data=$request->except('_token');
        if($request->hasFile('pan')) {
            $filename = $this->getFileName($request->pan);
            $request->pan->move(base_path('public/PanCardImage'), $filename);
            $data['pan']=$filename;
        }
        if($request->hasFile('cheque')) {
            $filename = $this->getFileName($request->cheque);
            $request->cheque->move(base_path('public/ChequeImage'), $filename);
            $data['cheque']=$filename;
        }

        $data['agent_id']=Auth::guard('agent')->user()->id;

        if($agntprofile){
            $agntprofile->update($data);
        }
        else{
            AgentProfile::create($data);
        }

        return redirect('/agent/dashboard')->with('flash_message','Profile updated Successfully');

    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }

    public function ifsccode(Request $request)
    {
            $ifcs_code   = $request['code'];
        try {
            $url = "https://mfapps.indiatimes.com/ET_Calculators/getBankDetailsByIfsc.htm?ifsccode=".$ifcs_code;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $data = curl_exec($ch);
            $about = json_decode($data, TRUE);
            $branch=strtoupper(str_replace('-',' ',$about['branch']));
            $bankname=strtoupper(str_replace('-',' ',$about['bankname']));
            $bankdetail[]=['branch'=>$branch,'bankname' =>$bankname];
            return $bankdetail;


        } catch (\Exception $e) {
            return 0;
        }
    }

    public function completeuserprofile(){
        $agent=Auth::guard('agent')->user();
        $agntprofile=AgentProfile::where('agent_id',$agent->id)->first();
        return view ('agent.completeuserprofile',compact('agent','agntprofile'));
    }

    public function saveprofileimage(Request $request){
        $agent=Auth::guard('agent')->user();
        if($request->hasFile('profile_image')) {
            $filename = $this->getFileName($request->profile_image);
            $request->profile_image->move(base_path('public/images/AgentProfile'), $filename);
            $agent->profile_image=$filename;
            $agent->save();
            return 1;
        }
        else{
            return 0;
        }


    }

    public function showpiechart(Request $request){
        $data=$request->all();


    }



}
