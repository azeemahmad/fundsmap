<?php

namespace App\Http\Controllers\Agent\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Agent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('agent_guest')->except('logout');
    }
    protected function guard()
    {
        return Auth::guard('agent');
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/agent/dashboard';

    public function agentlogin(){

        return view ('agent.auth.login');
    }

    public function mobile_verification(Request $request)
    {
        $this->validate($request, [
            'countryCode' => 'required',
            'mobile' => 'required|digits:10',
        ]);
        $data=$request->all();
        $user=Agent::where('mobile_code',$data['countryCode'])->where('mobile',$data['mobile'])->first();
        if(!$user){
            $userdata['mobile_code']=$request['countryCode'];
            $userdata['mobile']=$request['mobile'];
            $user=Agent::create($userdata);
        }
        $otp = rand(10000, 99999);
        //file_get_contents('http://www.smszone.in/sendsms.asp?page=SendSmsBulk&username=pmsecurities&password=4c60&number=' .'+'.$user->mobile_code.$user->mobile.'&message=' . urlencode("Please use below otp to verify your mobile number in Fundsmap.  ".$otp) .'&senderid=SCOUTQ');
        Session::put('otp',$otp);
        $user->otp=$otp;
        $user->save();
        return view('agent.auth.mobileverify',compact('data'));
    }
    public function otp_verification(Request $request)
    {
        $data=$request->all();
        $otp=Session::get('otp');
        $user=Agent::where('mobile_code',$data['mobilecode'])->where('mobile',$data['mobile'])->first();
        if(!$user){
            echo 0;
        }
        elseif($otp != $user->otp){
            echo 1;
        }
        elseif($data['otp'] != $user->otp ){
            echo 2;
        }
        else{
            $user->mobile_verified_status=1;
            $user->mobile_verified_at=date('Y-m-d h:i:s');
            $user->save();
            Session::forget('otp');
            Session::put('userdetails',$user->mobile_code.'|'.$user->mobile);
            echo 3;
        }

    }

    public function user_register(){

        $data= explode('|',Session::get('userdetails'));
        $user=Agent::where('mobile_code',$data[0])->where('mobile',$data[1])->where('mobile_verified_status',1)->first();
        if($user){
            if($user->first_name != null && $user->last_name != null){
                $this->guard()->login($user);
                return redirect($this->redirectTo);
            }

        }
        else{
           return redirect('/login')->with('error_message','Please verify your mobile first !');
        }
        return view('agent.auth.register',compact('user'));
    }

   public function save_user_register(Request $request){
   
       $this->validate($request, [
           'first_name' => 'required|string',
           'last_name' => 'required|string',
           'city_id' => 'required',
           'state_id' => 'required',
           'country_id' => 'required',
           'password' => ['required', 'string', 'min:8'],
           'term&condition' =>'required'
       ]);
       $userdata= explode('|',Session::get('userdetails'));
       $user=Agent::where('mobile_code',$userdata[0])->where('mobile',$userdata[1])->where('mobile_verified_status',1)->first();
       if($user){
           $data=$request->all();
           $data['password']=bcrypt($data['password']);
           $user->update($data);
           Session::forget('userdetails');
           $this->guard()->login($user);
           return redirect($this->redirectTo);

       }
       else{
           return redirect('/login')->with('error_message','Please verify your mobile first !');
       }
   }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        return redirect('/')->with('flash_message', 'Logout Successfully !');
    }

}
