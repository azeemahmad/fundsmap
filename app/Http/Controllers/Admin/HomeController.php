<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use ConsoleTVs\Charts\Facades\Charts;
use App\User;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $blockbuster=10;
        $superhit=35;
        $hit=67;
        $average=20;
        $flop=78;

        $per=73;
        $rem=27;
        $name='Amitab Bachchan';

        $chart=Charts::create('pie', 'highcharts')
            ->title('K-Mean Clustering Algorithm for All Movie')
            ->labels(['Blockbuster','SuperHit', 'Hit', 'Average','Flop'])
            ->colors(['#00ff80', '#00ffff','#bf00ff', '#ffff00','#ff8000'])
            ->values([$blockbuster,$superhit,$hit,$average,$flop])
            ->dimensions(750,300)
            ->responsive(false);

        $prediction=Charts::create('bar', 'highcharts')
            ->title('Decision Making Algorithm for Future Movie')
            ->labels(['Successful','Unsuccessful'])
            ->colors(['#125c0e','#ff0000'])
            ->values([$per,$rem])
            ->dimensions(750,300)
            ->responsive(false);

        $user = Charts::database(User::all(), 'bar', 'highcharts')
            ->title('List of Users')
            ->elementLabel("Total")
            ->dimensions(750, 300)
            ->responsive(false)
            ->lastByDay(15, true);

        $donut=Charts::create('donut', 'highcharts')
            ->title('My donut chart')
            ->labels(['First', 'Second', 'Third'])
            ->values([5,10,20])
            ->dimensions(750,300)
            ->responsive(false);

        return view('admin.home',compact('chart','prediction','name','user','donut'));
    }
    public function profile()
    {
        return view('admin.profile');
    }

    public function saveprofile(Request $request)
    {
        $user_list = Auth::user();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            ]);
        } else {
            if (!Hash::check($request['old_password'], $user_list->password)) {
                return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
            }

            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
                'password' => 'required|string|min:6|confirmed|different:old_password',
                'old_password' => 'required|string|min:6'
            ]);
        }
        $data = $request->all();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'image' => $filename
                ]);
            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                ]);
            }

        } else {

            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'image' => $filename
                ]);

            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password'])
                ]);
            }
        }
        return redirect('/admin/profile')->with('flash_message', 'Profile updated Successfully!');
    }
    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }
}
