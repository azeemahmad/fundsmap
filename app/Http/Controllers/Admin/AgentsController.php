<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Agent;
use App\Models\AgentProfile;
use Illuminate\Http\Request;
use App\Authorizable;

class AgentsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $agents = Agent::orWhere(function ($q) use($keyword)
            {
                $q->where('first_name', 'LIKE', "%$keyword%")
                    ->orWhere('last_name', 'LIKE', "%$keyword%")
                    ->orWhere('email', 'LIKE', "%$keyword%")
                    ->orWhere('mobile', 'LIKE', "%$keyword%");
            })->where('mobile_verified_status',1)->latest()->paginate($perPage);
        } else {
            $agents = Agent::where('mobile_verified_status',1)->paginate($perPage);
        }



        return view('admin.agents.index', compact('agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.agents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'mobile_code' => 'required',
            'mobile' => 'required',
        ]);

        $requestData = $request->all();

        Agent::create($requestData);

        return redirect('admin/agents')->with('flash_message', 'Agent added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $agent = Agent::findOrFail($id);
        $agntprofile=AgentProfile::where('agent_id',$id)->first();

        return view('admin.agents.show', compact('agent','agntprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $agent = Agent::findOrFail($id);

        return view('admin.agents.edit', compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'mobile_code' => 'required',
            'mobile' => 'required',
        ]);
        $requestData = $request->all();

        $agent = Agent::findOrFail($id);
        $agent->update($requestData);

        return redirect('admin/agents')->with('flash_message', 'Agent updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Agent::destroy($id);

        return redirect('admin/agents')->with('flash_message', 'Agent deleted!');
    }
}
