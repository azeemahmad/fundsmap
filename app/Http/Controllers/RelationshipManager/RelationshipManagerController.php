<?php

namespace App\Http\Controllers\RelationshipManager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class RelationshipManagerController extends Controller
{
    public function clientmanagement(){
        return view('relationship-manager.clientmanagement');
    }
    public function relationshipmanger(){
        return view('relationship-manager.relationshipmanger');
    }


}