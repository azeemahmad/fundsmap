<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Agent\IndexController@index');

/******************  START  CLIENT ROUTE::    ***********************************************/
Route::group(['prefix' => 'client', 'namespace' => 'Client\Auth', 'middleware' => 'client_guest'], function () {
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('client.register');
    Route::post('/register', 'RegisterController@register')->name('client.register');
    Route::get('/login', 'LoginController@showLoginForm')->name('client.login');
    Route::post('/login', 'LoginController@postlogin')->name('client.login');
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('/password/reset/{id}', 'ResetPasswordController@showResetForm')->name('client.password.reset');
    Route::post('/password/reset/{id}', 'ResetPasswordController@reset')->name('client.password.update');
});

Route::group(['prefix' => 'client', 'namespace' => 'Client', 'middleware' => 'client_auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('client.logout');
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
});

/******************  END  CLIENT ROUTE::    ***********************************************/



/******************  START  Agent ROUTE::    ***********************************************/

Route::group(['prefix' => 'agent', 'namespace' => 'Agent','middleware' => 'agent_guest'] , function () {
    Route::get('/login','Auth\LoginController@agentlogin');
    Route::post('/mobile_verification','Auth\LoginController@mobile_verification')->name('mobile.verification');
    Route::post('/otp_verification','Auth\LoginController@otp_verification');
    Route::get('/user_register','Auth\LoginController@user_register');
    Route::post('/user_register','Auth\LoginController@save_user_register')->name('user.register');
    Route::get('/state','IndexController@state');
    Route::get('/city','IndexController@city');
});
Route::group(['prefix' => 'agent', 'namespace' => 'Agent','middleware' => 'agent_auth'] , function () {
    Route::get('/dashboard','HomeController@dashboard');
    Route::get('/home', 'HomeController@dashboard');
    Route::get('/userlogout','Auth\LoginController@logout');
    Route::get('/userprofile','HomeController@userprofile');
    Route::get('/completeuserprofile','HomeController@completeuserprofile');
    Route::post('/userprofile','HomeController@saveuserprofile');
    Route::post('/saveprofileimage','HomeController@saveprofileimage');
    Route::post('/ifsccode','HomeController@ifsccode');
    Route::get('/showpiechart','HomeController@showpiechart');
});
/******************  END  Agent ROUTE::    ***********************************************/


/******************  START ADMIN ROUTE::    ***********************************************/
Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'guest'], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'auth'], function () {
    Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['auth', 'verified']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/agents', 'AgentsController');
});
/******************  END ADMIN ROUTE::    ***********************************************/


/******************  START Relation Manger ROUTE::    ************************************/
Route::group(['prefix' => 'relationship-manager', 'namespace' => 'RelationshipManager','middleware' =>'rmanager_guest'], function () {

    Route::get('/client-manager', 'RelationshipManagerController@clientmanagement');
    Route::get('/relationship-manager', 'RelationshipManagerController@relationshipmanger');

});

Route::group(['prefix' => 'relationship-manager', 'namespace' => 'RelationshipManager','middleware' =>'rmanager_auth'], function () {


});

/******************  END Relation Manger ROUTE::    ************************************/

